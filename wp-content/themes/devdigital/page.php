<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php
	include "lib/inc/page-header.php";
	?>
	<?php get_template_part( 'content', 'single' ); ?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

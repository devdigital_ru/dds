<div class="content marginbottom">
	<ul class="sale-list">
		<?php
		// задаем нужные нам критерии выборки данных из БД
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'sale',
			'post_status' => 'publish'
		);

		$query = new WP_Query( $args );

		// Цикл
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				?>
				<li>
					<a href="<?= get_the_permalink() ?>"><h2><?= get_the_title() ?></h2></a>
					<a href="#" class="open-short-text">Показать текст</a>
					<a href="<?= get_the_permalink() ?>" class="readmore">Читать подробнее</a>
					<div class="short-description"><?= get_field('sale_short_description') ?></div>
				</li>
		<?php
			}
		}
		/* Возвращаем оригинальные данные поста. Сбрасываем $post. */
		wp_reset_postdata();
		?>


	</ul>

	<?php /*	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/three.js/r54/three.min.js"></script>
	<div class="generateSale">
		<header>
			<hgroup>
				<h2>Конфигуратор акций</h2>
				<h3>Вы не знаете какую из акций выбрать? Воспользуйтесь мной или свяжитесь с ребятами из студии</h3>
			</hgroup>
		</header>

		<div id="actionGenerate">
			<div id="wrapGenerate">
				<div id="generateText">This test text</div>
			</div>
		</div>


	</div>
	*/ ?>

</div>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php
	include "lib/inc/page-header.php";
	?>
	<?php
	$args = array(
		'post_type'      => 'projects',
		'post_status' => 'publish',
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);
	if ( $query->have_posts() ) {
		echo '</div><div class="container-fluid"><div class="row project-list">';
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part('content', 'projects');
		}
		echo '</div></div><div class="container">';
	}
	wp_reset_postdata();
	?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

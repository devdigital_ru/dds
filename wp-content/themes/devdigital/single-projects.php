<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php
	include "lib/inc/page-header.php";
	?>
<?php endwhile; // end of the loop. ?>
</div>
</main>
<?php
function titleDescription($title, $description)
{
	return '<h2>' . $title . '</h2><div class="description">' . $description . '</div>';
}

function mediaContent($media)
{
	switch ( $media["acf_fc_layout"] ) {
		case "image":
			$image_url = wp_get_attachment_image_src($media["image"], 1000, 1000);
			return '<div class="image" style="background-image: url(\'' . $image_url[0] . '\')"></div>';
			break;
		case "prototype-maket":
			return '<div class="twentytwenty-container">
					<img src="' . $media["image-prototype"]["url"] . '">
					<img src="' . $media["image-maket"]["url"] . '">
					</div>';
			break;
	}
}

?>
<div class="container-inner-project container-fluid marginbottom">
	<?php
	$image_url = get_field('project_preview');
	if ( $image_url ) {
		?>
		<div class="image-project">
			<img src="<?= $image_url['url'] ?>" alt="<?= get_field('project_name') ?>"
			     title="<?= get_field('project_name') ?>">
		</div>
	<?php } ?>
	<div class="main-information-project">
		<div class="row">
			<div class="col-md-4 left-column">
				<time class="date-project"><?= mysql2date(get_option('date_format'), get_the_date('c')) ?></time>
				<p class="name-client"><?= get_the_title() ?></p>
				<div class="share">
					<div>
						<!-- Sharingbutton Facebook -->
						<a class="resp-sharing-button__link"
						   href="https://facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>&amp;t=<?= get_the_title() ?>"
						   target="_blank"
						   aria-label="">
							<div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton VK -->
						<a class="resp-sharing-button__link"
						   href="http://vk.com/share.php?url=<?= get_the_permalink() ?>&amp;title=<?= get_the_title() ?>&amp;description=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>&amp;image=<?= $image_url['url'] ?>"
						   target="_blank"
						   aria-label="">
							<div class="resp-sharing-button resp-sharing-button--vk resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M21.547 7h-3.29a.743.743 0 0 0-.655.392s-1.312 2.416-1.734 3.23C14.734 12.813 14 12.126 14 11.11V7.603A1.104 1.104 0 0 0 12.896 6.5h-2.474a1.982 1.982 0 0 0-1.75.813s1.255-.204 1.255 1.49c0 .42.022 1.626.04 2.64a.73.73 0 0 1-1.272.503 21.54 21.54 0 0 1-2.498-4.543.693.693 0 0 0-.63-.403h-2.99a.508.508 0 0 0-.48.685C3.005 10.175 6.918 18 11.38 18h1.878a.742.742 0 0 0 .742-.742v-1.135a.73.73 0 0 1 1.23-.53l2.247 2.112a1.09 1.09 0 0 0 .746.295h2.953c1.424 0 1.424-.988.647-1.753-.546-.538-2.518-2.617-2.518-2.617a1.02 1.02 0 0 1-.078-1.323c.637-.84 1.68-2.212 2.122-2.8.603-.804 1.697-2.507.197-2.507z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton Twitter -->
						<a class="resp-sharing-button__link"
						   href="https://twitter.com/intent/tweet/?text=<?= get_the_title() ?>&amp;url=<?= get_the_permalink() ?>"
						   target="_blank" aria-label="">
							<div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton Google+ -->
						<a class="resp-sharing-button__link"
						   href="https://plus.google.com/share?url=<?= get_the_permalink() ?>" target="_blank"
						   aria-label="">
							<div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3" />
									</svg>
								</div>
							</div>
						</a>
					</div>
					<div>
						<!-- Sharingbutton Tumblr -->
						<a class="resp-sharing-button__link"
						   href="https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=<?= get_the_title() ?>&amp;caption=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>&amp;content=<?= get_the_permalink() ?>&amp;canonicalUrl=<?= get_the_permalink() ?>&amp;shareSource=tumblr_share_button"
						   target="_blank" aria-label="">
							<div class="resp-sharing-button resp-sharing-button--tumblr resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M13.5.5v5h5v4h-5V15c0 5 3.5 4.4 6 2.8v4.4c-6.7 3.2-12 0-12-4.2V9.5h-3V6.7c1-.3 2.2-.7 3-1.3.5-.5 1-1.2 1.4-2 .3-.7.6-1.7.7-3h3.8z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton Pinterest -->
						<a class="resp-sharing-button__link"
						   href="https://pinterest.com/pin/create/button/?url=<?= get_the_permalink() ?>&amp;media=<?= $image_url['url'] ?>&amp;description=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>"
						   target="_blank" aria-label="">
							<div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M12.14.5C5.86.5 2.7 5 2.7 8.75c0 2.27.86 4.3 2.7 5.05.3.12.57 0 .66-.33l.27-1.06c.1-.32.06-.44-.2-.73-.52-.62-.86-1.44-.86-2.6 0-3.33 2.5-6.32 6.5-6.32 3.55 0 5.5 2.17 5.5 5.07 0 3.8-1.7 7.02-4.2 7.02-1.37 0-2.4-1.14-2.07-2.54.4-1.68 1.16-3.48 1.16-4.7 0-1.07-.58-1.98-1.78-1.98-1.4 0-2.55 1.47-2.55 3.42 0 1.25.43 2.1.43 2.1l-1.7 7.2c-.5 2.13-.08 4.75-.04 5 .02.17.22.2.3.1.14-.18 1.82-2.26 2.4-4.33.16-.58.93-3.63.93-3.63.45.88 1.8 1.65 3.22 1.65 4.25 0 7.13-3.87 7.13-9.05C20.5 4.15 17.18.5 12.14.5z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton Telegram -->
						<a class="resp-sharing-button__link"
						   href="https://telegram.me/share/url?text=<?= get_the_title() ?>&amp;<?= get_the_permalink() ?>"
						   target="_blank" aria-label="">
							<div class="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M.707 8.475C.275 8.64 0 9.508 0 9.508s.284.867.718 1.03l5.09 1.897 1.986 6.38a1.102 1.102 0 0 0 1.75.527l2.96-2.41a.405.405 0 0 1 .494-.013l5.34 3.87a1.1 1.1 0 0 0 1.046.135 1.1 1.1 0 0 0 .682-.803l3.91-18.795A1.102 1.102 0 0 0 22.5.075L.706 8.475z" />
									</svg>
								</div>
							</div>
						</a>
						<!-- Sharingbutton E-Mail -->
						<a class="resp-sharing-button__link"
						   href="mailto:?subject=<?= get_the_title() ?>&amp;body=<?= get_the_permalink() ?>"
						   target="_self" aria-label="">
							<div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--small">
								<div aria-hidden="true"
								     class="resp-sharing-button__icon resp-sharing-button__icon--solid">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M22 4H2C.9 4 0 4.9 0 6v12c0 1.1.9 2 2 2h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zM7.25 14.43l-3.5 2c-.08.05-.17.07-.25.07-.17 0-.34-.1-.43-.25-.14-.24-.06-.55.18-.68l3.5-2c.24-.14.55-.06.68.18.14.24.06.55-.18.68zm4.75.07c-.1 0-.2-.03-.27-.08l-8.5-5.5c-.23-.15-.3-.46-.15-.7.15-.22.46-.3.7-.14L12 13.4l8.23-5.32c.23-.15.54-.08.7.15.14.23.07.54-.16.7l-8.5 5.5c-.08.04-.17.07-.27.07zm8.93 1.75c-.1.16-.26.25-.43.25-.08 0-.17-.02-.25-.07l-3.5-2c-.24-.13-.32-.44-.18-.68s.44-.32.68-.18l3.5 2c.24.13.32.44.18.68z" />
									</svg>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-8 right-column">
				<h2><?= get_field('project_name') ?></h2>
				<?php $site_link = get_field('project_site');
				if ( $site_link ) { ?>
				<a href="<?= $site_link ?>" target="_blank" rel="nofollow" class="link-site button-link">перейти на
						сайт</a><?php
				} ?>
				<?php
				$posttags = get_the_tags();
				if ( $posttags ) {
					echo '<ul class="tags">';
					foreach ($posttags as $tag) {
						?>
						<li><a href="#"><?= $tag->name ?></a></li>
						<?php
					}
					echo '</ul>';
				}
				?>
				<div class="description"><?= get_field('project_fulldescription'); ?></div>
			</div>
		</div>
	</div>
	<?php
	$project_description = get_field('project_project');
	if ( $project_description ) {
		foreach ($project_description as $pd) {
			$background = '';
			$background .= $pd['background_image'] ? 'url(' . $pd['background_image']['url'] . ') ' : '';
			$background .= $pd['background_color'] ? $pd['background_color'] . ' ' : '';
			$background .= $pd['background_position'] ? $pd['background_position'] . ' ' : '';
			$background .= $pd['background_repeat'] ? $pd['background_repeat'] . ' ' : '';
			$background .= $pd['background_size'] ? $pd['background_size'] . ' ' : '';
			?>
			<div class="information-project" <?= $background ? 'style="background: ' . $background . '"' : '' ?>>
				<?php
				if ( $pd["block_info"] ) {
					foreach ($pd["block_info"] as $bi) {
						switch ( $bi['position'] ) {
							case 'left':
								?>
								<div class="wrap-block-info row">
									<div class="col-md-4 info-block right">
										<?= titleDescription($bi['title'], $bi['description']) ?>
									</div>
									<div class="col-md-8 image-block">
										<?= mediaContent($bi['media'][0]) ?>
									</div>
								</div>
								<?php
								break;
							case 'right':
								?>
								<div class="wrap-block-info row">
									<div class="col-md-8 image-block">
										<?= mediaContent($bi['media'][0]) ?>
									</div>
									<div class="col-md-4 info-block left">
										<?= titleDescription($bi['title'], $bi['description']) ?>
									</div>
								</div>
								<?php
								break;
							case 'center':
								?>
								<div class="wrap-block-info row noflex">
									<div class="col-md-12 info-block center">
										<?= titleDescription($bi['title'], $bi['description']) ?>
									</div>
									<div class="col-md-12 image-block">
										<?= mediaContent($bi['media'][0]) ?>
									</div>
								</div>
								<?php
								break;
						}
					}
				}
				?>
			</div>
			<?php
		}
	}
	?>
	<?php
	$layouts = get_field('layouts');
	if ( $layouts && !$is_IE ) {
		?>
		<div class="layouts-adaptive-project">
			<?php
			$layout_PC = $layout_macbook = $layout_tablet = $layout_phone = false;
			foreach ($layouts as $lay) {
				if ( $lay["PC"] )
					$layout_PC = true;
				if ( $lay["macbook"] )
					$layout_macbook = true;
				if ( $lay["laptop"] )
					$layout_tablet = true;
				if ( $lay["phone"] )
					$layout_phone = true;
			}
			if ( $layout_PC || $layout_macbook || $layout_tablet || $layout_phone ) {
				?>
				<ul class="view">
					<?php if ( $layout_PC ) { ?>
						<li class="active" data-swicth="PC"><?= get_field('PC', 'option') ?></li>
					<?php } ?>
					<?php if ( $layout_macbook ) { ?>
						<li <?= !$layout_PC ? 'class="active"' : '' ?> data-swicth="Notebook"><?= get_field('notebook', 'option') ?></li>
					<?php } ?>
					<?php if ( $layout_tablet ) { ?>
						<li <?= (!$layout_PC && !$layout_macbook) ? 'class="active"' : '' ?> data-swicth="Plane">
							<?= get_field('tablet', 'option') ?>
						</li>
					<?php } ?>
					<?php if ( $layout_phone ) { ?>
						<li <?= (!$layout_PC && !$layout_macbook && !$layout_tablet) ? 'class="active"' : '' ?>
							data-swicth="Phone"><?= get_field('telephone', 'option') ?>
						</li>
					<?php } ?>
				</ul>
			<?php } ?>
			<div class="wrap row">
				<div class="col-md-3 info-mockup">
					<ul class="page">
						<?php
						foreach ($layouts as $key => $lay) {
							?>
							<li<?= $key == 0 ? ' class="active"' : '' ?> data-index="<?= $key ?>"><span class="name"><?=
									$lay["layout_name"] ?></span>
								<span
									class="number"><?php if ( ($key + 1) < 10 ) echo '0' . ($key + 1); else echo ($key+ 1); ?></span>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
				<div class="col-md-9 design-view">
					<div class="mockup-pc">
						<?php if ( $layout_PC ) { ?>
							<!--		iMAC			-->
							<div class="device active" data-swicth="PC">
								<svg viewBox="10 122 980 770" preserveAspectRatio="xMinYMin meet" class="pc">
									<g>
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="498" cy="142.5"
										        r="3.8" />
										<g>
											<foreignObject x="0" y="0" width="1155" height="650" preserveAspectRatio
											               transform="matrix(0.7614 0 0 0.7614 58.0533 161.4387)"
											               overflow="scroll">
												<div class="layouts-wrap">
													<div class="dragscroll">
														<?php
														foreach ($layouts as $key => $lay) {
															?>
															<img src="<?= $lay["PC"]['url'] ?>"
															     data-index="<?= $key ?>"
															     style="width: 100%;height: auto;">
														<?php } ?>
													</div>
												</div>
											</foreignObject>
										</g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M947.9,781.3H48.1c-15.7,0-28.5-12.8-28.5-28.5V150.9
		c0-15.7,12.8-28.5,28.5-28.5h899.9c15.7,0,28.5,12.8,28.5,28.5v601.9C976.4,768.5,963.7,781.3,947.9,781.3z" />
										<rect x="58.4" y="161.8" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="878.6"
										      height="494.2" />
										<line fill="none" stroke="#929292" stroke-miterlimit="10" x1="19.9" y1="692.5"
										      x2="976.2"
										      y2="692.5" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M614.9,878l36.2,8.3l1.4,0.3c0,0-0.5,0.8-1.3,0.8
		c-13.2,0.2-120.2,0.7-152.8,0.7c-38.8,0-136.1-0.5-151.2-0.7c-1.3,0-2-0.8-2-0.8l37-8.5c1.7-0.4,3.1-1.6,3.7-3.3
		c0.8-2.2,1.9-5.8,3.2-11.4c4.8-20.7,8.6-82.5,8.6-82.5h202.3c0,0,3.6,59.6,8.6,83.6c0.9,4.2,2.5,8.6,3.5,11
		C612.6,877,613.6,877.8,614.9,878z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M652.3,887.2c0,0-0.2,2.5-2.3,2.7c-13.1,0.9-120.2,0.7-152.8,0.7
		c-38.8,0-133-0.2-148.2-0.7c-4.2-0.2-4.3-2.9-4.3-2.9" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_macbook ) { ?>
							<!--		MACBOOK			-->
							<div class="device<?= !$layout_PC ? ' active' : '' ?>" data-swicth="Notebook">
								<svg x="0px" y="0px" viewBox="224 342 550 315" preserveAspectRatio="xMinYMin meet"
								     class="notebook">
									<g>
										<g>
											<foreignObject x="0" y="0" width="821" height="513" preserveAspectRatio
											               transform="matrix(0.4994 0 0 0.4994 293.75 363)"
											               overflow="hidden">
												<div class="layouts-wrap">
													<div class="dragscroll">
														<?php
														foreach ($layouts as $key => $lay) {
															?>
															<img src="<?= $lay["macbook"]['url'] ?>"
															     data-index="<?= $key ?>"
															     style="width: 100%;height: auto;">
														<?php } ?>
													</div>
												</div>
											</foreignObject>
										</g>
										<g id="camera_1_">
											<g>
												<ellipse fill="#FFFFFF" cx="499" cy="353.5" rx="2" ry="2" />
											</g>
											<g>
												<ellipse fill="none" stroke="#929292" stroke-miterlimit="10" cx="499"
												         cy="353.5"
												         rx="2" ry="2" />
											</g>
										</g>
										<rect x="293.8" y="363" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="410"
										      height="256.3" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M717.2,639.1H281.3c-1.2,0-2.1-3.4-2.1-4.6v-275
		c0-8.6,7-15.7,15.7-15.7h408.9c8.6,0,15.7,7,15.7,15.7v275C719.3,635.6,718.3,639.1,717.2,639.1z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M767.4,648.4H230.7c-0.8,0-1.4-0.6-1.4-1.4v-6.5
		c0-0.8,0.6-1.4,1.4-1.4h536.7c0.8,0,1.4,0.6,1.4,1.4v6.5C768.8,647.8,768.2,648.4,767.4,648.4z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M748.3,655.2H249.5c-3.8,0-19.4-3.1-19.4-6.9l0,0H768l0,0
		C768,652.1,752,655.2,748.3,655.2z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M530.6,643.8h-63.3c-2.5,0-4.5-2-4.5-4.5V639h72.3v0.3
		C535.1,641.8,533,643.8,530.6,643.8z" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_tablet ) { ?>
							<!--		iPAD			-->
							<div class="device<?= (!$layout_PC && !$layout_macbook) ? ' active' : '' ?>"
							     data-swicth="Plane">
								<svg x="0px" y="0px"
								     viewBox="275 180 400 640" preserveAspectRatio="xMinYMin meet" class="ipad">
									<g>
										<g>
											<foreignObject x="0" y="0" width="381" height="509" preserveAspectRatio
											               transform="matrix(1 0 0 1 308.0154 246.4975)">
												<div class="layouts-wrap">
													<div class="dragscroll" id="plane">
														<?php
														foreach ($layouts as $key => $lay) {
															?>
															<img src="<?= $lay["laptop"]['url'] ?>"
															     data-index="<?= $key ?>"
															     style="width: 100%;height: auto;">
														<?php } ?>
													</div>
												</div>
											</foreignObject>
										</g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M693.3,811H305.1c-14,0-25.4-11.4-25.4-25.4V214.2
		c0-14,11.4-25.4,25.4-25.4h388.2c14,0,25.4,11.4,25.4,25.4v571.4C718.7,799.6,707.3,811,693.3,811z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M692.9,807.9H305.5c-12.5,0-22.5-10.1-22.5-22.5V214.5
		c0-12.5,10.1-22.5,22.5-22.5h387.3c12.5,0,22.5,10.1,22.5,22.5v570.9C715.4,797.8,705.3,807.9,692.9,807.9z" />
										<rect x="309.3" y="247.8" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="379.3"
										      height="507.3" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="499" cy="218.5"
										        r="3" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="498.9"
										        cy="782.7"
										        r="14.2" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_phone ) { ?>
							<!--		iPhone			-->
							<div
								class="device<?= (!$layout_PC && !$layout_macbook && !$layout_tablet) ? ' active' : '' ?>"
								data-swicth="Phone">
								<svg x="0px" y="0px" class="phone" viewBox="395 295 200 410"
								     preserveAspectRatio="xMinYMin meet">
									<g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M570.7,701.5H429.3c-16.2,0-29.3-13.1-29.3-29.3V327.8
		c0-16.2,13.1-29.3,29.3-29.3h141.3c16.2,0,29.3,13.1,29.3,29.3v344.4C600,688.4,586.9,701.5,570.7,701.5z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M571,698.3H427.5c-13.9,0-25.2-11.3-25.2-25.2V326.7
		c0-13.9,11.3-25.2,25.2-25.2H571c13.9,0,25.2,11.3,25.2,25.2V673C596.3,686.9,584.9,698.3,571,698.3z" />
										<g>
											<foreignObject x="410" y="348" width="177.5" height="305.5"
											               preserveAspectRatio>
												<div class="layouts-wrap">
													<div class="dragscroll" id="phone">
														<?php
														foreach ($layouts as $key => $lay) {
															?>
															<img src="<?= $lay["phone"]['url'] ?>"
															     data-index="<?= $key ?>"
															     style="width: 100%;height: auto;">
														<?php } ?>
													</div>
												</div>
											</foreignObject>
										</g>
										<circle fill="#929292" cx="498.9" cy="312.1" r="2" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="467.9" cy="324"
										        r="3" />
										<path fill="#929292" d="M513.4,325h-26.9c-0.8,0-1.5-0.7-1.5-1.5l0,0c0-0.8,0.7-1.5,1.5-1.5h26.9c0.8,0,1.5,0.7,1.5,1.5l0,0
		C514.9,324.3,514.2,325,513.4,325z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M600.9,413.6H600v-29.1h0.9c0.4,0,0.7,0.3,0.7,0.7v27.6
		C601.7,413.3,601.3,413.6,600.9,413.6z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,413.3h1.8v-29.1h-1.8c-0.4,0-0.7,0.3-0.7,0.7v27.6
		C397.4,413,397.8,413.3,398.2,413.3z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,450.4h1.8v-29.1h-1.8c-0.4,0-0.7,0.3-0.7,0.7v27.6
		C397.4,450,397.8,450.4,398.2,450.4z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,365.7h1.8V349h-1.8c-0.4,0-0.7,0.3-0.7,0.7V365
		C397.4,365.4,397.8,365.7,398.2,365.7z" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="499.8"
										        cy="676.7"
										        r="14.9" />
									</g>
								</svg>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php
	$stage_work = get_field('stage_work');
	if ( $stage_work ) {
		?>
		<div class="cd-horizontal-timeline">
			<div class="container">
				<header>
					<hgroup>
						<h2><?= get_field('stage_work_title') ?></h2>
						<h3><?= get_field('stage_work_header_description') ?></h3>
					</hgroup>
				</header>
			</div>
			<div class="timeline">
				<div class="events-wrapper">
					<div class="events">
						<ol>
							<?php
							$count = count($stage_work);
							foreach ($stage_work as $key => $sw) {
								?>
								<li><a href="#0" data-date="<?= ($key + 1) ?>/01/2014"
								       class="older-event<?= $key == 0 ? ' selected' : '' ?>"><?= ($key + 1) ?></a></li>
							<?php } ?>
						</ol>
						<span class="filling-line" aria-hidden="true"></span>
					</div> <!-- .events -->
				</div> <!-- .events-wrapper -->
				<ul class="cd-timeline-navigation">
					<li><a href="#0" class="prev inactive">Prev</a></li>
					<li><a href="#0" class="next">Next</a></li>
				</ul> <!-- .cd-timeline-navigation -->
			</div> <!-- .timeline -->
			<div class="events-content">
				<ol>
					<?php
					foreach ($stage_work as $key => $sw) {
						?>
						<li<?= $key == 0 ? ' class="selected"' : '' ?> data-date="<?= ($key + 1) ?>/01/2014">
							<div class="wrap">
								<?= $sw['title'] ? '<h2>' . $sw['title'] . '</h2>' : '' ?>
								<?= $sw['description'] ? '<div class="description">' . $sw['description'] . '</div>' : '' ?>
								<?= $sw['image'] ? '<div class="image">' . wp_get_attachment_image($sw['image']['ID'],
										array(1100, 2000)) . '</div>' : '' ?>
							</div>
						</li>
						<?php
					}
					?>
				</ol>
			</div> <!-- .events-content -->
		</div>
	<?php } ?>

	<?php
	$review_project = get_field('project_review');
	if ( $review_project ) {
		?>
		<div class="client-project">
			<div class="container">
				<?php echo reviewView($review_project->ID); ?>
			</div>
		</div>
	<?php } ?>
	<div class="tell-aboutus-project">
		<h2><?= get_field('you_like_project', 'option') ?></h2>
		<p><?= get_field('description_field_like_project', 'option') ?></p>
		<div class="social">
			<!-- Sharingbutton Facebook -->
			<a class="resp-sharing-button__link"
			   href="https://facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>&amp;t=<?= get_the_title() ?>"
			   target="_blank" aria-label="Facebook">
				<div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" />
						</svg>
					</div>
					Facebook
				</div>
			</a>
			<!-- Sharingbutton Twitter -->
			<a class="resp-sharing-button__link"
			   href="https://twitter.com/intent/tweet/?text=<?= get_the_title() ?>&amp;url=<?= get_the_permalink() ?>"
			   target="_blank" aria-label="Twitter">
				<div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" />
						</svg>
					</div>
					Twitter
				</div>
			</a>
			<!-- Sharingbutton Google+ -->
			<a class="resp-sharing-button__link" href="https://plus.google.com/share?url=<?= get_the_permalink() ?>"
			   target="_blank" aria-label="Google+">
				<div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3" />
						</svg>
					</div>
					Google+
				</div>
			</a>
			<!-- Sharingbutton Tumblr -->
			<a class="resp-sharing-button__link"
			   href="https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=<?= get_the_title() ?>&amp;caption=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>&amp;content=<?= get_the_permalink() ?>&amp;canonicalUrl=<?= get_the_permalink() ?>&amp;shareSource=tumblr_share_button"
			   target="_blank" aria-label="Tumblr">
				<div class="resp-sharing-button resp-sharing-button--tumblr resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M13.5.5v5h5v4h-5V15c0 5 3.5 4.4 6 2.8v4.4c-6.7 3.2-12 0-12-4.2V9.5h-3V6.7c1-.3 2.2-.7 3-1.3.5-.5 1-1.2 1.4-2 .3-.7.6-1.7.7-3h3.8z" />
						</svg>
					</div>
					Tumblr
				</div>
			</a>
			<!-- Sharingbutton E-Mail -->
			<a class="resp-sharing-button__link"
			   href="mailto:?subject=<?= get_the_title() ?>&amp;body=<?= get_the_permalink() ?>" target="_self"
			   aria-label="E-Mail">
				<div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M22 4H2C.9 4 0 4.9 0 6v12c0 1.1.9 2 2 2h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zM7.25 14.43l-3.5 2c-.08.05-.17.07-.25.07-.17 0-.34-.1-.43-.25-.14-.24-.06-.55.18-.68l3.5-2c.24-.14.55-.06.68.18.14.24.06.55-.18.68zm4.75.07c-.1 0-.2-.03-.27-.08l-8.5-5.5c-.23-.15-.3-.46-.15-.7.15-.22.46-.3.7-.14L12 13.4l8.23-5.32c.23-.15.54-.08.7.15.14.23.07.54-.16.7l-8.5 5.5c-.08.04-.17.07-.27.07zm8.93 1.75c-.1.16-.26.25-.43.25-.08 0-.17-.02-.25-.07l-3.5-2c-.24-.13-.32-.44-.18-.68s.44-.32.68-.18l3.5 2c.24.13.32.44.18.68z" />
						</svg>
					</div>
					E-Mail
				</div>
			</a>
			<!-- Sharingbutton Pinterest -->
			<a class="resp-sharing-button__link"
			   href="https://pinterest.com/pin/create/button/?url=<?= get_the_permalink() ?>&amp;media=<?= $image_url['url'] ?>&amp;description=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>"
			   target="_blank" aria-label="Pinterest">
				<div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M12.14.5C5.86.5 2.7 5 2.7 8.75c0 2.27.86 4.3 2.7 5.05.3.12.57 0 .66-.33l.27-1.06c.1-.32.06-.44-.2-.73-.52-.62-.86-1.44-.86-2.6 0-3.33 2.5-6.32 6.5-6.32 3.55 0 5.5 2.17 5.5 5.07 0 3.8-1.7 7.02-4.2 7.02-1.37 0-2.4-1.14-2.07-2.54.4-1.68 1.16-3.48 1.16-4.7 0-1.07-.58-1.98-1.78-1.98-1.4 0-2.55 1.47-2.55 3.42 0 1.25.43 2.1.43 2.1l-1.7 7.2c-.5 2.13-.08 4.75-.04 5 .02.17.22.2.3.1.14-.18 1.82-2.26 2.4-4.33.16-.58.93-3.63.93-3.63.45.88 1.8 1.65 3.22 1.65 4.25 0 7.13-3.87 7.13-9.05C20.5 4.15 17.18.5 12.14.5z" />
						</svg>
					</div>
					Pinterest
				</div>
			</a>
			<!-- Sharingbutton LinkedIn -->
			<a class="resp-sharing-button__link"
			   href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?= get_the_permalink() ?>&amp;title=<?= get_the_title() ?>&amp;source=<?= get_the_permalink() ?>"
			   target="_blank" aria-label="LinkedIn">
				<div class="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M6.5 21.5h-5v-13h5v13zM4 6.5C2.5 6.5 1.5 5.3 1.5 4s1-2.4 2.5-2.4c1.6 0 2.5 1 2.6 2.5 0 1.4-1 2.5-2.6 2.5zm11.5 6c-1 0-2 1-2 2v7h-5v-13h5V10s1.6-1.5 4-1.5c3 0 5 2.2 5 6.3v6.7h-5v-7c0-1-1-2-2-2z" />
						</svg>
					</div>
					LinkedIn
				</div>
			</a>
			<!-- Sharingbutton WhatsApp -->
			<a class="resp-sharing-button__link"
			   href="whatsapp://send?text=<?= get_the_title() ?>.<?= get_the_permalink() ?>" target="_blank"
			   aria-label="WhatsApp">
				<div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M20.1 3.9C17.9 1.7 15 .5 12 .5 5.8.5.7 5.6.7 11.9c0 2 .5 3.9 1.5 5.6L.6 23.4l6-1.6c1.6.9 3.5 1.3 5.4 1.3 6.3 0 11.4-5.1 11.4-11.4-.1-2.8-1.2-5.7-3.3-7.8zM12 21.4c-1.7 0-3.3-.5-4.8-1.3l-.4-.2-3.5 1 1-3.4L4 17c-1-1.5-1.4-3.2-1.4-5.1 0-5.2 4.2-9.4 9.4-9.4 2.5 0 4.9 1 6.7 2.8 1.8 1.8 2.8 4.2 2.8 6.7-.1 5.2-4.3 9.4-9.5 9.4zm5.1-7.1c-.3-.1-1.7-.9-1.9-1-.3-.1-.5-.1-.7.1-.2.3-.8 1-.9 1.1-.2.2-.3.2-.6.1s-1.2-.5-2.3-1.4c-.9-.8-1.4-1.7-1.6-2-.2-.3 0-.5.1-.6s.3-.3.4-.5c.2-.1.3-.3.4-.5.1-.2 0-.4 0-.5C10 9 9.3 7.6 9 7c-.1-.4-.4-.3-.5-.3h-.6s-.4.1-.7.3c-.3.3-1 1-1 2.4s1 2.8 1.1 3c.1.2 2 3.1 4.9 4.3.7.3 1.2.5 1.6.6.7.2 1.3.2 1.8.1.6-.1 1.7-.7 1.9-1.3.2-.7.2-1.2.2-1.3-.1-.3-.3-.4-.6-.5z" />
						</svg>
					</div>
					WhatsApp
				</div>
			</a>
			<!-- Sharingbutton VK -->
			<a class="resp-sharing-button__link"
			   href="http://vk.com/share.php?title=<?= get_the_permalink() ?>&amp;title=<?= get_the_title() ?>&amp;description=<?= get_the_title() ?>. <?= get_field('project_fulldescription'); ?>&amp;image=<?= $image_url['url'] ?>"
			   target="_blank" aria-label="VK">
				<div class="resp-sharing-button resp-sharing-button--vk resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M21.547 7h-3.29a.743.743 0 0 0-.655.392s-1.312 2.416-1.734 3.23C14.734 12.813 14 12.126 14 11.11V7.603A1.104 1.104 0 0 0 12.896 6.5h-2.474a1.982 1.982 0 0 0-1.75.813s1.255-.204 1.255 1.49c0 .42.022 1.626.04 2.64a.73.73 0 0 1-1.272.503 21.54 21.54 0 0 1-2.498-4.543.693.693 0 0 0-.63-.403h-2.99a.508.508 0 0 0-.48.685C3.005 10.175 6.918 18 11.38 18h1.878a.742.742 0 0 0 .742-.742v-1.135a.73.73 0 0 1 1.23-.53l2.247 2.112a1.09 1.09 0 0 0 .746.295h2.953c1.424 0 1.424-.988.647-1.753-.546-.538-2.518-2.617-2.518-2.617a1.02 1.02 0 0 1-.078-1.323c.637-.84 1.68-2.212 2.122-2.8.603-.804 1.697-2.507.197-2.507z" />
						</svg>
					</div>
					VK
				</div>
			</a>
			<!-- Sharingbutton Telegram -->
			<a class="resp-sharing-button__link"
			   href="https://telegram.me/share/url?text=<?= get_the_title() ?>&amp;url=<?= get_the_permalink() ?>"
			   target="_blank" aria-label="Telegram">
				<div class="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--medium">
					<div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path
								d="M.707 8.475C.275 8.64 0 9.508 0 9.508s.284.867.718 1.03l5.09 1.897 1.986 6.38a1.102 1.102 0 0 0 1.75.527l2.96-2.41a.405.405 0 0 1 .494-.013l5.34 3.87a1.1 1.1 0 0 0 1.046.135 1.1 1.1 0 0 0 .682-.803l3.91-18.795A1.102 1.102 0 0 0 22.5.075L.706 8.475z" />
						</svg>
					</div>
					Telegram
				</div>
			</a>
		</div>
	</div>
	<div class="progect-navigation">
		<div class="container">
			<?php _mbbasetheme_post_nav_image(); ?>
		</div>
	</div>
</div>
<main role="main">
	<div class="container">
		<?php get_footer(); ?>

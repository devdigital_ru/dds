<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _mbbasetheme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title><?php wp_title(' :: ', true, 'right'); ?></title>
	<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/devdigital/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/devdigital/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/devdigital/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/devdigital/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/devdigital/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/devdigital/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/devdigital/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/devdigital/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/devdigital/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/devdigital/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/devdigital/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/devdigital/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/devdigital/favicon/favicon-16x16.png">
	<link rel="manifest" href="/wp-content/themes/devdigital/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#29c5b9">
	<meta name="msapplication-TileImage" content="/wp-content/themes/devdigital/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#29c5b9">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="background-gradient">
<div class="loader" id="loader">
	<div class="span">
		<div class="line-rotate">
			<div class="line-loader-bar"></div>
		</div>
	</div>
	<div class="span span2">
		<div class="line-rotate">
			<div class="line-loader-bar"></div>
		</div>
	</div>
</div>
<header id="header">
	<nav class="navbar navbar-light">
		<span class="background-menu"></span>
		<div class="navbar-brand">
			<a href="/">
			<span class="logo-icon">
			<svg x="0px" y="0px"
			     viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
<polygon fill="#010202" points="48,71.3 30.8,71.3 73.8,26 91,26 " />
				<polygon fill="#010202" points="28.2,71.3 11,71.3 35.1,45.9 52.3,45.9 " />
</svg>
				</span>
			</a>
			<input type="checkbox" id="spinner-form" />
			<label for="spinner-form" class="spinner-spin">
				<div class="spinner diagonal part-1"></div>
				<div class="spinner horizontal"></div>
				<div class="spinner diagonal part-2"></div>
			</label>
		</div>
		<?php
		$args = array(
			'theme_location'  => '',
			'menu'            => 'main',
			'container'       => false,
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'nav navbar-nav',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => '',
		);
		wp_nav_menu($args); ?>
		<?php the_widget('qTranslateXWidget', array('type' => 'text', 'hide-title' => true, 'widget-css' => false)); ?>
	</nav>
	<?php the_widget('qTranslateXWidget', array('type' => 'text', 'hide-title' => true, 'widget-css' => false)); ?>
</header>
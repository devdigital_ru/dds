<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php
	include "lib/inc/page-header.php";
	?>
	<?php get_template_part('content', 'sale'); ?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

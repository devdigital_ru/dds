window.onload = function () {
	window.ShapesPolyfill.run();


};

$( window ).load(function() {
	$( "#loader" ).animate({
		opacity: 0
	}, 1000, function() {
		document.getElementById('loader').remove();
	});

});

$(document).ready(function () {

	if (!document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Extensibility', '1.1')) {
		d3.select(".inner-page-title svg .textWrap .text_field")
			.append('text')
			.attr('class', 'text-uppercase title')
			.attr('font-size', 25)
			.attr('x', 34)
			.text(foreignDontWork.title);

	var description_split = foreignDontWork.description.split('***');

	if (description_split.length) {
		var j = 0,
			y = 12;
		for (i = description_split.length - 1; i >= 0; i--) {
			d3.select(".inner-page-title svg .textWrap .text_field")
				.append('text')
				.attr('class', 'text-uppercase description')
				.attr('font-size', 10)
				.attr('x', 25 - j * 10)
				.attr('y', y)
				.text(description_split[j]);
			j++;
			y *= 2;
		}
	}

	}

	$('.spinner-spin').on('click', function () {
		//$('.navbar-brand').toggleClass('active');

		$(this).css({'pointer-events': 'none', 'opacity': '.5'});

		if ($("#spinner-form").is(':checked')) {
			$('#header .navbar').removeClass('visible');
			$(".background-menu").stop(true, true).animate({width: '0'}, {
				complete: function () {
					$('.spinner-spin').css({'pointer-events': 'auto', 'opacity': '1'});
				}
			});
		} else {
			$(".background-menu").stop(true, true).animate({width: '100%'}, {
				complete: function () {
					$('#header .navbar').addClass('visible');
					$('.spinner-spin').css({'pointer-events': 'auto', 'opacity': '1'});
				}
			});

		}

	});

	$('.widget.qtranxs_widget').click(function (e) {
		if (!$(this).hasClass('open')) {
			e.preventDefault();
			$(this).addClass('open');
		} else {
			$(this).removeClass('open');
		}
	});

	$(document).mouseup(function (e) {
		var div = $(".language-chooser.language-chooser-text.qtranxs_language_chooser");
		if (!div.is(e.target) && div.has(e.target).length === 0) {
			$('.widget.qtranxs_widget').removeClass('open');
		}
	});

	$('.hello-block .hover_group, .inner-page-title .hover_group, .button-open-form').click(function (e) {
		e.preventDefault();
		$('.contact-form-image').click();
	});

	$('.callbackForm .contact-form-image, .mobile-label').click(function (e) {
		e.preventDefault();
		if ($('.callbackForm .form').hasClass('open')) {
			$('.callbackForm .form').removeClass('open')
		} else {
			$('.callbackForm .form').addClass('open')
		}
	});

});

function contactformBlockHeightEvent() {
	var width = window.innerWidth,
		height = window.innerHeight;

	//alert('width='+width+' height='+height);

	if (width > 767 && height > 600)
		$('.callbackForm .form').css('max-width', $('.callbackForm .form').height() * 0.45);
	else
		$('.callbackForm .form').css('max-width', $('.callbackForm .form').height() * 0.8);


//	TITLE INNER PAGE
	if ($('.inner-page-title').length)
		$('.inner-page-title').css('height', width * 0.5);

}

function contactformBlockHeight() {
	$(window).resize(contactformBlockHeightEvent);
}


$(document).ready(function () {
	contactformBlockHeightEvent();
	contactformBlockHeight();
});
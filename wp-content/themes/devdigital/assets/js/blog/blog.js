$(document).ready(function(){
	console.log('WORKPAGE');
	$('body').readRemaining({
		showGaugeDelay   : 1000,           // Delay before showing the indicator.
		showGaugeOnStart : true,          // Show the gauge initially, even before the user scroll.
		timeFormat       : '%mмин.', // Will replace %m and %s with minutes and seconds.
		maxTimeToShow    : 20*60,          // Only show time if is lower than x minutes (multiplied to seconds).
		minTimeToShow    : 10             // Only show time if is higher than x seconds (If it's less than 10 seconds... just read).

	});

	var t = false;

	if ( $.type(getCookie( $('article.post').attr('id'))) !== "undefined" ) {
		$('.last-read').css('opacity', 1);
		setTimeout("$('.last-read').css('opacity', 0)", 10000);
		setTimeout("deleteCookie( $('article.post').attr('id') )", 10000);
	}

	$('.last-read').click(function(){
		if ( $.type(getCookie( $('article.post').attr('id'))) !== "undefined" ) {
			$('body').animate({'scrollTop' : getCookie( $('article.post').attr('id'))}, 500);
			$(this).css('opacity', 0);
			deleteCookie($('article.post').attr('id') );
		}
	});


	$(document).scroll(function(){

		if ( $.type(getCookie( $('article.post').attr('id'))) === "undefined" ) t = true;
		if (t && $('body').scrollTop() > 200)//$(window).height() &&  $(document).height() - $('body').scrollTop())
			setCookie($('article.post').attr('id'), $('body').scrollTop(), {expires : 2678400, path : document.URL});
	});
});


if ($('.service-variants').length > 0) {

	$('.brief-form').each(function (i, elem) {

		var item = $(this).data('item');
		$("#brief-form" + item).animatedModal({
			color: '#01d5d9',
			//color:'#000',
			beforeOpen: function () {
				$('#animatedModal .modal-content').find('#brief-form-' + item).removeClass('hidden');
				console.log("The animation was called");
			},
			//afterOpen: function() {
			//    console.log("The animation is completed");
			//},
			//beforeClose: function() {
			//    console.log("The animation was called");
			//},
			afterClose: function () {
				$('#animatedModal .modal-content').find('#brief-form-' + item).addClass('hidden');
				console.log("The animation is completed");
			}
		});
	});

	//$("#itsme0").animatedModal();
	//$("#itsme1").animatedModal();
	//$("#itsme2").animatedModal();
}
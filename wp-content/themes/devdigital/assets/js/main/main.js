var container, stats;
var camera, scene, renderer;
var cube, plane;
var targetRotation = 0;
var targetRotationOnMouseDown = 0;
var mouseX = 0;
var mouseXOnMouseDown = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
init();
animate();

function init() {
	container = document.getElementById('card-block');
	//document.body.appendChild(container);
	var info = document.createElement('div');
	info.setAttribute("id", "cardElement");
	container.appendChild(info);

	//CAMERA
	camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 1, 1000);
	camera.position.y = 150;
	camera.position.z = 500;

	// SCENE
	scene = new THREE.Scene();

	// LIGHT
	//var light = new THREE.PointLight();
	var light = new THREE.SpotLight();
	light.position.set(0, 200, 500);
	light.castShadow = true;
	scene.add(light);

	//// Plane
	var geometry = new THREE.PlaneGeometry(400, 200, 10, 10);
	geometry.rotateX(-Math.PI / 2);
	var material = new THREE.MeshLambertMaterial({
		map: THREE.ImageUtils.loadTexture('/wp-content/themes/devdigital/assets/images/vis/shadow.png'),
		shading: THREE.SmoothShading,
		transparent: true,
		opacity: 1
	});
	plane = new THREE.Mesh(geometry, material);
	plane.receiveShadow = true;
	scene.add(plane);

	// MATERIAL FOR CUBE
	var materialArray = [];
	materialArray.push(new THREE.MeshLambertMaterial({
		color: 0x000000,
		overdraw: true
	}));
	materialArray.push(new THREE.MeshLambertMaterial({
		color: 0x000000,
		overdraw: true
	}));
	materialArray.push(new THREE.MeshLambertMaterial({
		color: 0x000000,
		overdraw: true
	}));
	materialArray.push(new THREE.MeshLambertMaterial({
		color: 0x000000,
		overdraw: true
	}));
	materialArray.push(new THREE.MeshLambertMaterial({
		map: THREE.ImageUtils.loadTexture('/wp-content/themes/devdigital/assets/images/vis/01.jpg'),
		overdraw: true
	}));
	materialArray.push(new THREE.MeshLambertMaterial({
		map: THREE.ImageUtils.loadTexture('/wp-content/themes/devdigital/assets/images/vis/03.jpg'),
		overdraw: true
	}));

	// CUBE
	var geometry = new THREE.CubeGeometry(400, 220, 2, 15, 15, 15),
		material = new THREE.MeshPhongMaterial({vertexColors: THREE.FaceColors, overdraw: 0.5}),
		meshFaceMaterial = new THREE.MeshFaceMaterial(materialArray);
	cube = new THREE.Mesh(geometry, meshFaceMaterial);
	cube.position.set(0, 150, 0);
	cube.castShadow = true;
	cube.receiveShadow = true;
	scene.add(cube);

	renderer = new THREE.WebGLRenderer({alpha: true});
	renderer.setClearColor(0xffffff, 0);
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	info.appendChild(renderer.domElement);
	//stats = new Stats();
	//info.appendChild(stats.dom);
	var el = document.getElementById('card-block');
	el.addEventListener('mousedown', onDocumentMouseDown, false);
	el.addEventListener('touchstart', onDocumentTouchStart, false);
	el.addEventListener('touchmove', onDocumentTouchMove, false);
	window.addEventListener('resize', onWindowResize, false);
}
function onWindowResize() {
	windowHalfX = window.innerWidth / 2;
	windowHalfY = window.innerHeight / 2;
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}
function onDocumentMouseDown(event) {
	event.preventDefault();
	var el = document.getElementById('card-block');
	el.addEventListener('mousemove', onDocumentMouseMove, false);
	el.addEventListener('mouseup', onDocumentMouseUp, false);
	el.addEventListener('mouseout', onDocumentMouseOut, false);
	mouseXOnMouseDown = event.clientX - windowHalfX;
	targetRotationOnMouseDown = targetRotation;
}
function onDocumentMouseMove(event) {
	mouseX = event.clientX - windowHalfX;
	targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.02;
}
function onDocumentMouseUp(event) {
	var el = document.getElementById('card-block');
	el.removeEventListener('mousemove', onDocumentMouseMove, false);
	el.removeEventListener('mouseup', onDocumentMouseUp, false);
	el.removeEventListener('mouseout', onDocumentMouseOut, false);
}
function onDocumentMouseOut(event) {
	var el = document.getElementById('card-block');
	el.removeEventListener('mousemove', onDocumentMouseMove, false);
	el.removeEventListener('mouseup', onDocumentMouseUp, false);
	el.removeEventListener('mouseout', onDocumentMouseOut, false);
}
function onDocumentTouchStart(event) {
	if (event.touches.length === 1) {
		//event.preventDefault();
		mouseXOnMouseDown = event.touches[0].pageX - windowHalfX;
		targetRotationOnMouseDown = targetRotation;
	}
}
function onDocumentTouchMove(event) {
	if (event.touches.length === 1) {
		//event.preventDefault();
		mouseX = event.touches[0].pageX - windowHalfX;
		targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.05;
	}
}
function animate() {
	requestAnimationFrame(animate);
	//stats.begin();
	render();
	//stats.end();
}
function render() {
	plane.rotation.y = cube.rotation.y += ( targetRotation - cube.rotation.y ) * 0.01;
	renderer.render(scene, camera);
}


window.onload = function() {
	var canSvg = !!(document.createElementNS && document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect);
	if(!canSvg){
		window.location = document.location.protocol+"//m."+document.location.host;
	}
};

function helloBlockHeightEvent() {
	var width = window.innerWidth,
		height = window.innerHeight;
	$('.hello-block').css('height', width * 0.67);
	$('.aboutus-block').css('height', width * 0.68);
	if (width < 544) {
		$('.hello-block').css('height', width * 1.5);
		$('.aboutus-block').css('height', width * 1.6);
	}

}


function helloBlockHeight() {
	$(window).resize(helloBlockHeightEvent);
}

$(document).ready(function () {
	helloBlockHeightEvent();
	helloBlockHeight();
});

$(document).ready(function () {

	$('#mainAbout .hover_group').click(function(){
		window.location = document.location.protocol+document.location.host+"/we/";
	});

	if (!document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Extensibility', '1.1')) {
		// если не поддерживается foreign object
		// HELLO BLOCK
		d3.select(".hello-block svg .textWrap .text_field")
			.append('text')
			.attr('class', 'text-uppercase title')
			.attr('font-size', 28)
			.attr('x', 34)
			.text(foreignDontWork.helloblock[0].title);

		var description_split = foreignDontWork.helloblock[0].description.split('***');

		if (description_split.length) {
			var j = 0,
				y = 15;
			for (i = description_split.length - 1; i >= 0; i--) {
				d3.select(".hello-block svg .textWrap .text_field")
					.append('text')
					.attr('class', 'text-uppercase description')
					.attr('font-size', 15)
					.attr('x', 12 * i)
					.attr('y', y)
					.text(description_split[j]);
				j++;
				y *= 2;
			}
		}

	//	ABOUT US
		var description = foreignDontWork.aboutus[0].description,
			new_str = [],
			array_space = [];

		var target = " "; // цель поиска

		var pos = 0;
		// вхождения в подстроку
		while (true) {
			var foundPos = description.indexOf(target, pos);
			if (foundPos == -1) break;
			array_space.push(foundPos);
			pos = foundPos + 1;
		}

		pos = 0;
		var max_lenght = 85,
			current_lenght = max_lenght,
			last_max = 0,
			last_length = 0,
			count_save = 0;
		// щем по длине ближайший символ и обрезаем
		for (var i = 0; i < array_space.length; i++) {
			if (array_space[i] > last_max && array_space[i] < current_lenght)
			// если текущая длина больше сохраненной и меньше допустимой длины
				last_max = array_space[i];
			if (i == array_space.length - 1 || array_space[i + 1] > current_lenght) {
				// если последний элемент  или следующая длина больше максимального значения
				if (i == array_space.length - 1) {
					new_str.push(description.substring(last_length + 1, description.length));
				} else {
					if (count_save != 0)
						new_str.push(description.substring(last_length + 1, last_max));
					else
						new_str.push(description.substring(last_length, last_max));
					last_length = array_space[i];
					current_lenght += max_lenght;
					count_save++;
				}
			}
		}

		// добавляем программно заголовок
		d3.select(".aboutus-block svg .textWrap .text_field")
			.append('text')
			.attr('class', 'text-uppercase title')
			.text(foreignDontWork.aboutus[0].title);

		if(new_str.length){
			d3.select(".aboutus-block svg .textWrap .text_field")
				.append('text')
				.attr('class', 'description');
			var x= -6;
				y = 10;

			for(i=0; i<new_str.length; i++){
				d3.select(".aboutus-block svg .textWrap .text_field .description")
					.append('tspan')
					.attr('x', x + (i*-10))
					.attr('y', y+(i*10+1))
					.text(new_str[i]);
			}
		}


	}
});
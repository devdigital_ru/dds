if ($('.item-itsme').length > 0) {

	$('.itsme-popup').each(function (i, elem) {

		var item = $(this).data('item');
		$("#itsme" + item).animatedModal({
			color: '#01d5d9',
			//color:'#000',
			beforeOpen: function () {
				$('#animatedModal .modal-content').find('#itsme-more-' + item).removeClass('hidden');
			},
			//afterOpen: function() {
			//    console.log("The animation is completed");
			//},
			//beforeClose: function() {
			//    console.log("The animation was called");
			//},
			afterClose: function () {
				$('#animatedModal .modal-content').find('#itsme-more-' + item).addClass('hidden');
			}
		});
	});

	//$("#itsme0").animatedModal();
	//$("#itsme1").animatedModal();
	//$("#itsme2").animatedModal();
}


// init images
var images = [];
$('.itsme .item-itsme .small-image').each(function () {
	var offset = $(this).offset();
	var center_top = Math.round(offset.top + $(this).height() / 2);
	var center_left = Math.round(offset.left + $(this).width() / 2);

	images.push([center_top, center_left]);
});

$(document).ready(function () {

	$('.wordcard .words-list a span').hover(function () {
		$('.words-wrap .element').addClass('gray-color');
		var activeID = $(this).data('active-words');
		$('.words-wrap .element span[data-word="' + activeID + '"]').addClass('active-word');
	}, function () {
		$('.words-wrap .element span').removeClass('active-word');
		$('.words-wrap .element').removeClass('gray-color');
	});

	//$('.wordcard .words-list a').click(function(event){
	//    event
	//});


	$('.certificates').magnificPopup({
		delegate: 'a', // child items selector, by clicking on it popup will open
		type: 'image',
		gallery: {
			enabled: true
		}
	});


	$('.email-social-list .social-icon').hover(function () {
		var color = $(this).data('color');

		$(this).css({
			backgroundColor: color,
			borderColor: color
		}, 1500);
	}, function () {

		$(this).css({
			backgroundColor: 'transparent',
			borderColor: '#ffffff'
		}, 1500);
	});


	//$('.naoTooltip-wrap').naoTooltip({speed: 200});

	$('.words-list').click(function (e) {
		e.preventDefault();
	});

});

var x = null;
var y = null;

document.addEventListener('mousemove', onMouseUpdate, false);
document.addEventListener('mouseenter', onMouseUpdate, false);

function onMouseUpdate(e) {
	x = e.pageX;
	y = e.pageY;
	$('.item-itsme').each(function (i, obj) {
		var item_element = $('#itsme' + i + ' .small-image'),
			cursor_top = y,
			cursor_left = x,
			element_top = item_element.offset().top,
			element_left = item_element.offset().left,
			element_width = item_element.width(),
			element_height = item_element.height();

		var top_top = '-585px 0',
			top_left = '-868px 0',
			left_left = '-1155px 0',
			left_bottom = '-1442px 0',
			bottom_bottom = '-1730px 0',
			bottom_right = '-2017px 0',
			right_right = '0 0',
			right_top = '-292px 0';

		/*
		 * Проверка на верх элемента
		 */
		if (cursor_top < element_top) {
			item_element.css('background-position', top_top);
		}

		/*
		 * Проверка на низ элемента
		 */
		if (cursor_top > (element_top + element_height)) {
			item_element.css('background-position', bottom_bottom);
		}

		/*
		 * Левая проверка
		 */
		if (cursor_left < element_left) {
			item_element.css('background-position', left_left);
		}

		/*
		 * Правая проверка
		 */
		if (cursor_left > (element_left + element_width)) {
			item_element.css('background-position', right_right);
		}

		/*
		 * Левая верхняя диагональ
		 */
		if ((cursor_top < element_top && cursor_left < (element_left + element_width * 0.25)) ||
			(cursor_left < element_left && cursor_top < (element_top + element_height * 0.25))) {
			item_element.css('background-position', top_left);
		}

		/*
		 * Левая нижняя диагональ
		 */
		if ((cursor_top > (element_top + element_height * 0.75 ) && cursor_left < element_left) ||
			(cursor_top > (element_top + element_height) && (cursor_left < element_left + element_width * 0.25))) {
			item_element.css('background-position', left_bottom);
		}

		/*
		 * Правая нижняя диагональ
		 */
		if ((cursor_top > (element_top + element_height * 0.75 ) && cursor_left > element_left + element_width) ||
			(cursor_top > (element_top + element_height) && (cursor_left > element_left + element_width * 0.75))) {
			item_element.css('background-position', bottom_right);
		}

		/*
		 * Правая верхняя диагональ
		 */
		if ((cursor_left > element_left + element_width * 0.75 && cursor_top < element_top) ||
			(cursor_top < element_top + element_height * 0.25 && cursor_left > element_left + element_width)) {
			item_element.css('background-position', right_top);
		}
	});
}


$(window).load(function () {

	jQuery.fn.exists = function () {
		return this.length > 0;
	}

	var len;
	var random;

	var i = 0;
	while ($('.words-wrap .element.gray-color').exists()) {
		window.timer = setTimeout(function () {
			len = $('.words-wrap .element.gray-color').length;
			random = Math.floor(Math.random() * len) + 1;
			//console.log(random);
			$('.words-wrap .element.gray-color').eq(random).removeClass('gray-color').removeClass('transform');

			if ($('.words-wrap .element.gray-color').length == 1) {
				$('.words-wrap .element.gray-color').removeClass('gray-color').removeClass('transform');
			}

			if ($('.words-wrap .element.gray-color').length == 0) $('.words-list').css('visibility', 'visible');

		}, 2000);

		i++;
		if (i == 1000) break;
	}


	//if(!$('.words-wrap .element.gray-color').exists()){
	//    $('.words-list').css('visibility','visible');
	//}

});

// Летопись и всё такое
var startDate = new Date("1 Mar 2013 12:00");

function setCounters() {
	var nowDate = new Date(),
		diffSeconds = Math.round((nowDate.getTime() - startDate.getTime()) / 1000);

	$('.music').html(Math.round((diffSeconds * 0.0009375 * 1000 * 5 / 7)) / 1000);
	$('.code').html(Math.round((diffSeconds / 42 * 1000 * 5 / 7)) / 1000);
	$('.coffee').html(Math.round((diffSeconds * 0.000125 * 1000)) / 1000);
	$('.moon').html(Math.round((diffSeconds / 2360591.424 * 1000)) / 1000);
	$('.earth').html(Math.round((diffSeconds * 1766.6667) / 100000) + '·10⁵');
}
setInterval(setCounters, 1000);
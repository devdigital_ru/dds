$(document).ready(function () {
    $('.drive').click(function(e){
        //$('a[data-type="PATH"]').click();
        $('body').animate({ scrollTop : $('.map').offset().top-110 }, 900 );
        e.preventDefault();
    });
});

$(function () {
    var $context = $('.map_canvas'),
        $map = $('.map_canvas #map'),
        $controls = $('#map_controls'),
        position = new google.maps.LatLng(50.532919, 36.572284),
    // https://maps.google.com/maps?q=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F,+%D0%91%D0%B5%D0%BB%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+%D0%91%D0%B5%D0%BB%D0%B3%D0%BE%D1%80%D0%BE%D0%B4,+%D1%83%D0%BB%D0%B8%D1%86%D0%B0+%D0%9A%D0%BE%D1%80%D0%BE%D0%BB%D0%B5%D0%B2%D0%B0+2%D0%90,+%D0%BA%D0%BE%D1%80%D0%BF+2&hl=ru&ie=UTF8&ll=50.57254,36.572757&spn=0.007972,0.021136&sll=37.0625,-95.677068&sspn=40.59616,86.572266&t=h&hnear=%D1%83%D0%BB.+%D0%9A%D0%BE%D1%80%D0%BE%D0%BB%D0%B5%D0%B2%D0%B0,+2%D0%90,+%D0%91%D0%B5%D0%BB%D0%B3%D0%BE%D1%80%D0%BE%D0%B4,+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%91%D0%B5%D0%BB%D0%B3%D0%BE%D1%80%D0%BE%D0%B4,+%D0%91%D0%B5%D0%BB%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F&z=16&iwloc=A
        bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(50.532919, 36.572284),
            new google.maps.LatLng(50.532919, 36.572284)
        ),
        marker, map,
        markerArray = [],
        stepDisplay = new google.maps.InfoWindow(),
        rendererOptions = {
            map: map,
            suppressMarkers: true
        },
        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

    // Правильно стилизуем и вешаем стили
    function createMapThatShowsCompanyLocation() {

        var styles = [
                {
                    featureType: "poi",
                    elementType: 'all',
                    stylers: [
                        {visibility: "off"}
                    ]
                }, {
                    featureType: "road.highway",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                }
            ],

            options = {
                zoom: 8,
                scrollwheel: false,
                center: position,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                styles: styles
            };

        map = new google.maps.Map($map.get(0), options);
    }

    // Показываем крутой маркер DD
    function addCustomMarker() {
        var markerImage = '/wp-content/themes/devdigital/assets/images/marker.png',
            image;

        image = new google.maps.MarkerImage(
            markerImage,
            new google.maps.Size(64.0, 64.0),
            new google.maps.Point(0, 0),
            new google.maps.Point(32.0, 55.0)
        );

        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: 'Devdigital',
            icon: image,
            clickable: false,
            animation: google.maps.Animation.DROP
            // текст в попапе
        });
    }

    // Показываем тру кнопки и отрабатываем нажатия
    function addCustomMapTypeControls() {
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push($controls.get(0));

        $controls.find('a:not([data-type="PATH"])').click(function (event) {
            event.preventDefault();
            var $this = $(this);
            $this.addClass('active')
                .siblings()
                .not('[data-type="PATH"]').removeClass('active');

            if ($this.data('type') === 'HYBRID') {
                $context.addClass('satellite');
            } else {
                $context.removeClass('satellite');
            }
	        console.log(google.maps.MapTypeId[$this.data('type')]);
            map.setMapTypeId(google.maps.MapTypeId[$this.data('type')]);
        });

        $controls.find('a[data-type="PATH"]').click(function (event) {
            event.preventDefault();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success, error, {enableHighAccuracy: true});
            } else {
                error('not supported');
            }
        });

    }

    // Очищаем "как доехать" и лишние метки
    function clearMarkers(directionsDisplay) {
        if (directionsDisplay) {
            directionsDisplay.setMap(null);
        }
        for (i = 0; i < markerArray.length; i++) {
            markerArray[i].setMap(null);
        }
        markerArray = [];
        map.setCenter(position);
        map.setZoom(16);
    }

    // Ставим метки маршрута
    function setPathIcons(directionsDisplay, position) {
        if (directionsDisplay) {
            directionsDisplay.setMap(null);
        }

        var directionsService = new google.maps.DirectionsService(),
            request = {
                origin: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                destination: new google.maps.LatLng(50.571947, 36.57217800000001),
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

        directionsService.route(request, function (response, status) {

            if (status == google.maps.DirectionsStatus.OK) {

                var myRoute = response.routes[0].legs[0];

                directionsDisplay.setDirections(response);
                for (var i = 0; i < myRoute.steps.length; i++) {
                    var icon = "/wp-content/themes/devdigital/assets/images/marker-step.png";
                    if (i == 0) {
                        icon = "/wp-content/themes/devdigital/assets/images/marker-man.png";
                    }
                    var marker = new google.maps.Marker({
                        position: myRoute.steps[i].start_point,
                        map: map,
                        icon: icon
                    });

                    attachInstructionText(marker, myRoute.steps[i].instructions);
                    markerArray.push(marker);
                }
                google.maps.event.trigger(markerArray[0], "click");
            }

        });

        directionsDisplay.setMap(map);
    }

    // Показываем хитрожопые подсказки
    function attachInstructionText(marker, text) {
        google.maps.event.addListener(marker, 'click', function () {
            stepDisplay.setContent(text);
            stepDisplay.open(map, marker);
        });
    }

    // Добавляем крутой зум
    function addCustomZoomNavigation() {
        var $nav = $context.find('#map_navigation'),
            $handle = $nav.find('.handle'),
            $slider = $nav.find('.slider'),
            min = 2,
            max = 18 - min,
            $bar = $nav.find('.bar'),
            barHeight = $bar.height(),
            timeoutId;

        function updateLevel(percent) {

            var zoom = Math.floor(max * (percent / 100)) + min;
            zoom = Math.max(zoom, min);
            clearInterval(timeoutId);
            if (percent !== undefined) {
                if (zoom != map.getZoom()) {
                    timeoutId = setTimeout(function () {
                        map.setZoom(zoom);
                    }, 1000);
                }
            } else {
                percent = Math.round((map.getZoom() - min) / max * 100);
            }
            $nav.find('.level').height(percent + '%');
            $handle.css({
                top: -20 + 220 - (percent / 100) * barHeight
            });
        }

        map.controls[google.maps.ControlPosition.TOP_LEFT].push($nav.get(0));

        $nav
            .find('.plus').click(function (event) {
            event.preventDefault();
            if (map.getZoom() - min < max) {
                map.setZoom(map.getZoom() + 1);
                updateLevel();
            }
        }).end()
            .find('.minus').click(function (event) {
            event.preventDefault();
            var zoom = map.getZoom();
            if (zoom > min) {
                map.setZoom(zoom - 1);
                updateLevel();
            }
        });

        $handle.draggable({
            axis: 'y',
            scroll: false,
            drag: function (event, ui) {
                ui.position.top = Math.max(Math.min(ui.position.top, 200), -20);
                var percent = 100 - (ui.position.top + 20) / barHeight * 100;
                updateLevel(percent);
            }
        });

        $slider.click(function (event) {
            var percent = (event.pageY - $slider.offset().top ) / barHeight * 100;
            updateLevel(100 - percent);
        });

        google.maps.event.addListener(map, "zoom_changed", function () {
            updateLevel();
        });

        updateLevel();
        map.fitBounds(bounds);
    }

    // Отрабатываем, что пользователь разрешил найти себя
    function success(position) {
        var $this = $controls.find('a[data-type="PATH"]');
        $this.toggleClass('active');

        if ($this.hasClass('active'))
            setPathIcons(directionsDisplay, position);
        else
            clearMarkers(directionsDisplay)
    }

    // Пользователь запретил, у него паранойя ;(
    function error(msg) {
        console.log(arguments);
	    alert('You blocked current position. This dont\'t work');
    }

     //Вызываемся
    createMapThatShowsCompanyLocation();
    addCustomMarker();
    addCustomMapTypeControls();
    addCustomZoomNavigation();
});
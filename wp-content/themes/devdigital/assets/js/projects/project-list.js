$(document).ready(function(){
	$('.project-categories li').click(function(){
		var category = $(this).data("item");
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/devdigital/lib/inc/ajax-projects.php",
			data: {
				cat: category
			},
			success: function (data) {
				var result = jQuery.parseJSON(data);
				$('.project-list').empty().append(result);
			},
			error: function () {
				alert("Ошибка загрузки. Перезагрузите страницу");
			}
		});
	});
});
<?php
/**
 * @package _mbbasetheme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content content-sale row">
		<?php the_content() ?>
	</div>
	<div class="button-sale">
		<a href="#" class="button-open-form blue-button"><?= get_field('connect_with_us','option')?></a>
	</div>
</article>

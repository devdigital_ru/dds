<?php
for ($i = 0; $i < 15; $i++) {
	?>
	<div class="col-md-3 col-xs-12 col-sm-6 item">
		<?php
		$image = get_field('abrakadabra_background');
		$image = wp_get_attachment_image_src( $image['ID'], 500, 600 );
		?>
		<div class="image" style="background-image: url('<?= $image[0] ?>')">
			<a href="<?= get_the_permalink() ?>">
			<div class="description">
				<h2><?= get_the_title() ?></h2>
				<h3><?= get_field('abrakadabra_short_description') ?></h3>
			</div>
			</a>
		</div>
	</div>
<?php } ?>
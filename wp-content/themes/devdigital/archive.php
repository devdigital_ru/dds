<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _mbbasetheme
 */
get_header(); ?>

<?php
include "lib/inc/page-header.php";
global $paged;
if ( get_query_var('paged') )
	$my_page = get_query_var('paged');
else {
	if ( get_query_var('page') )
		$my_page = get_query_var('page');
	else
		$my_page = 1;
	set_query_var('paged', $my_page);
	$paged = $my_page;
}
$obj = get_queried_object();


$args = array(
	// other query params here,
	'paged'          => $my_page,
	'cat'            => $obj->term_id,
	'post_type'      => 'post',
	'posts_per_page' => 10
);
$my_query = new WP_Query($args);
?>
<?php if ($my_query->have_posts()) : ?>
<div class="blog-list">
	<div class="col-md-8 blog-list-article">
		<?php
		the_archive_title('<h2 class="title-before-blog">', '</h2>');
		?>
		<?php while ( $my_query->have_posts() ) :
			$my_query->the_post(); ?>
			<?php
			get_template_part('content', 'blog');
			?>
		<?php endwhile;
		wp_pagenavi(array('query' => $my_query));
		wp_reset_postdata();
		?>
	</div>
	<div class="col-md-4 sidebar-blog">
		<?php get_sidebar(); ?>
	</div>
	<?php else : ?>
		<?php get_template_part('content', 'none'); ?>
	<?php endif; ?>
	<?php
	?>
	<?php get_footer(); ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<?php
global $paged;
if ( get_query_var('paged') )
	$my_page = get_query_var('paged');
else {
	if ( get_query_var('page') )
		$my_page = get_query_var('page');
	else
		$my_page = 1;
	set_query_var('paged', $my_page);
	$paged = $my_page;
}
$args = array(
	// other query params here,
	'paged'          => $my_page,
	'post_type'      => 'post',
	'tag' => get_query_var('tag'),
	'posts_per_page' => 10
);
$my_query = new WP_Query($args);
include "lib/inc/page-header.php";
?>
<div class="blog-list">
	<div class="col-md-8 blog-list-article">
		<?php
		if ( $my_query->have_posts() ) {
			while ( $my_query->have_posts() ) : $my_query->the_post();
				get_template_part('content', 'blog');
				?>
			<?php endwhile;
			wp_pagenavi(array('query' => $my_query));
			wp_reset_postdata();
		} ?>
	</div>
	<div class="col-md-4 sidebar-blog">
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>

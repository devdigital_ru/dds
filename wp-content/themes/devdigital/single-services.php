<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<div id="primary" class="content-area marginbottom">
	<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			include "lib/inc/page-header.php";
			?>
			<?php get_template_part('content', 'service-inner'); ?>
			<?php
			if ( wp_get_post_parent_id(get_the_ID()) != 0 ) {
			?>
			<section class="service-navigation">
				<?php _mbbasetheme_post_nav(); ?>
			</section>
		<?php } ?>
		<?php endwhile; // end of the loop. ?>
	</main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>

<?php
/*
 * CONTENT IN SERVICE INNER
 */
?>
	</div>
<?php
if ( wp_get_post_parent_id(get_the_ID()) === 0 ) {
	function imageService($id, $link)
	{
		$image_preview = get_field('service_image_for_list', $id);
		$string = '<div class="col-md-6 image">';
		if ( $image_preview ) {
			$string .= '<a href="' . $link . '"><img src="' . $image_preview['sizes']['large'] . '"></a>';
		}
		$string .= '</div>';
		return $string;
	}

	?>
	<section class="services-list">
		<div class="container">
			<?php
			$args = array(
				'sort_order'   => 'ASC',
				'sort_column'  => 'post_title',
				'hierarchical' => 1,
				'exclude'      => '',
				'include'      => '',
				'meta_key'     => '',
				'meta_value'   => '',
				'authors'      => '',
				'child_of'     => 0,
				'parent'       => get_the_ID(),
				'exclude_tree' => '',
				'number'       => '',
				'offset'       => 0,
				'post_type'    => 'services',
				'post_status'  => 'publish',
			);
			$pages = get_pages($args);
			$i = 0;
			foreach ($pages as $post) {
				setup_postdata($post);
				$current_id = get_the_ID();
				$current_post = get_post($current_id);
				?>
				<div class="row wrap">
					<?php if ( $i % 2 != 0 ) echo imageService(get_the_ID(), get_the_permalink()); ?>
					<div class="col-md-6 text">
						<a href="<?= the_permalink() ?>"><h2><?= get_the_title() ?></h2></a>
						<div class="description"><?= get_field('service_description_preview') ?></div>
						<p class="readmore"><a class="blue-button" href="<?= the_permalink() ?>">Читать далее</a></p>
					</div>
					<?php if ( $i % 2 == 0 ) echo imageService(get_the_ID(), get_the_permalink()); ?>
				</div>
				<?php
				$i++;
			}
			wp_reset_postdata();
			?>
		</div>
	</section>
	<?php
} else {
	?>
	<section class="service-descripton">
		<div class="container">
			<div class="row description-block">
				<div class="image col-md-6">
					<?php
					$image = get_field('service_photo');
					if ( $image )
						echo '<img src="' . $image['sizes']['large'] . '">';
					?>
				</div>
				<div class="description col-md-6">
					<?= get_field('service_call') ?>
					<div class="button-wrap">
						<a href="#" class="button-open-form blue-button"><?= get_field('connect_with_us','option')?></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	$service_list = get_field('service_sale_list');
	if ( get_field('service_sale') && $service_list ) {
		?>
		<section class="service-variants">
			<div class="container">
				<header>
					<hgroup>
						<h2><?= get_field('service_sale_title') ?></h2>
						<h3><?= get_field('service_sale_description') ?></h3>
					</hgroup>
				</header>
				<ul class="variants row row-flex">
					<?php
					$count = count($service_list);
					foreach ($service_list as $key => $sl) {
						?>
						<li class="current-variant col-lg-<?= round(12 / $count) ?> col-md-6">
							<div class="variant-inner">
								<h3><?= $sl['name'] ?></h3>
								<div class="button-wrap">
									<a href="#animatedModal" data-item="<?= $key ?>" id="brief-form<?= $key ?>"
									   class="blue-button brief-form">Заполнить бриф</a>
									<?php
									if($sl['brief_link']){
										?>
									<div class="download_brief">
										<a href="<?= $sl['brief_link'] ?>" target="_blank">Скачать бриф</a>
									</div>
									<?php } ?>
								</div>
								<ul class="list">
									<?php
									foreach ($sl['list_sale'] as $list) {
										?>
										<li><?= $list['inner_service'] ?></li>
									<?php } ?>
								</ul>
							</div>



						</li>
					<?php } ?>
				</ul>
				<div id="animatedModal">
					<div id="closebt-container" class="close-animatedModal">
						<img class="closebt" src="<?= bloginfo('template_url') ?>/assets/images/closebt.svg">
					</div>
					<div class="modal-content container-fluid">
						<?php
						foreach ($service_list as $key => $sl) {
							?>
							<div id="brief-form-<?= $key ?>" class="brief-form-wrap row hidden">
								<div class="container">
								<?php echo do_shortcode( $sl['form'] ); ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	<?php
	$advantages_list = get_field('service_advantages_list');
	if ( get_field('service_advantages') && $advantages_list ) {
		?>
		<section class="service-advantages">
			<div class="container">
				<header>
					<hgroup>
						<h2><?= get_field('service_advantages_title') ?></h2>
						<h3><?= get_field('service_advantages_description') ?></h3>
					</hgroup>
				</header>
				<div class="row advantages">
					<?php
					foreach ($advantages_list as $al) {
						?>
						<div class="item-advantage col-md-4">
							<?php
							if ( $al['icon'] ) {
								?>
								<img src="<?= $al['icon']['url'] ?>">
							<?php } ?>
							<h3><?= $al['title'] ?></h3>
							<div class="description-advantages"><?= $al['description'] ?></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
	<?php } ?>
	<?php
	$reviews_list = get_field('service_reviews');
	if(get_field('service_reviews_block') && $reviews_list){
	?>
	<section class="service-reviews">
		<div class="container">
			<div class="row">
				<div class="reviews-slide col-lg-10 offset-lg-1 col-md-10 offset-md-1">
					<?php
					foreach($reviews_list as $rl){?>
					<div class="slide">
						<?= reviewView($rl->ID) ?>
					</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</section>
		<?php } ?>
	<?php
	$stage_list = get_field('service_steps');
//	if(get_field('service_steps_block') && $stage_list){
	if ( $stage_list ) {
		function imageStage($url)
		{
			return '<div class="image col-md-6"><img src="' . $url . '"></div>';
		}

		?>
		<section class="service-stage">
			<div class="container">
				<header>
					<hgroup>
						<h2><?= get_field('service_steps_title') ?></h2>
						<h3><?= get_field('service_steps_description') ?></h3>
					</hgroup>
				</header>
				<ul class="stage-list">
					<?php
					foreach ($stage_list as $key => $stage) {
						?>
						<li class="row">
							<?php
							if ( $key % 2 == 0 ) echo imageStage($stage['image']['url']);
							?>
							<div class="description col-md-6">
								<div class="number"><?= ($key + 1) ?></div>
								<div class="content">
									<h3><?= $stage['title'] ?></h3>
									<div class="subtitle"><?= $stage['description'] ?></div>
								</div>
							</div>
							<?php
							if ( $key % 2 != 0 ) echo imageStage($stage['image']['url']);
							?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</section>
	<?php } ?>
	<?php
	$qa_list = get_field('service_question_answer_list');
	if ( get_field('service_question_answer') && $qa_list ) {
		?>
		<section class="service-question-answer">
			<div class="container">
				<header>
					<hgroup>
						<h2><?= get_field('service_question_answer_title') ?></h2>
						<h3><?= get_field('service_question_answer_description') ?></h3>
					</hgroup>
				</header>
				<div class="row qa-form">
					<div class="question-anaswer col-lg-8 col-md-12">
						<ul class="row list">
							<?php
							foreach ($qa_list as $key => $qa) {
								?>
								<li class="col-md-6">
									<h3><?= ($key + 1) ?>. <?= $qa['question'] ?></h3>
									<h4><?= $qa['answer'] ?></h4>
								</li>
							<?php } ?>
						</ul>
					</div>
					<div class="form col-lg-4 col-md-12">
						<div class="form-item">
							<div class="form">
								<h3><?= get_field('form_title') ?></h3>
								<?= get_field('form_description') ?>
								<?php echo do_shortcode(get_field('form_qa')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	<div class="container">
<?php } ?>
<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>

<?php

global $post;

switch ($post->post_type) {
	case 'projects':
		include(TEMPLATEPATH.'/single-projects.php');
		break;
	case 'post':
		include(TEMPLATEPATH.'/single-blog.php');
		break;
	default:
		include(TEMPLATEPATH.'/single-none.php');
}

//
//  $post = $wp_query->post;
//
//  if (in_category('team')) {
//	  include(TEMPLATEPATH.'/single-team.php');
//  }
//  elseif (in_category('actions')) {
//	  include(TEMPLATEPATH.'/single-actions.php');
//  }
//  elseif (in_category('hairs_serv') || in_category('body_serv') || in_category('face_s')) {
//	  include(TEMPLATEPATH.'/single-services.php');
//  }elseif (in_category('stati')) {
//	  include(TEMPLATEPATH.'/single-statii.php');
//  }elseif (in_category('programmy')) {
//	  include(TEMPLATEPATH.'/single-programmy.php');
//  }
//  else {
//	  include(TEMPLATEPATH.'/single-all.php');
//  }


/*
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php _mbbasetheme_post_nav(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?> */ ?>
<?php get_footer(); ?>

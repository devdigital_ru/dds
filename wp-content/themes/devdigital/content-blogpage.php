<?php
$getID = get_the_ID();
global $post;
?>
<article id="post-<?= $getID ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		$image = get_the_post_thumbnail($getID, 'blog-preview');
		if ( $image ) {
			?>
			<section class="image-preview-post">
				<?= $image ?>
			</section>
			<?php
		}
		?>
		<h1 class="entry-title"><a href="<?= the_permalink() ?>"><?php the_title(); ?></a></h1>
		<section class="info-post">
			<p class="date"><?= date('d.m.Y', strtotime($post->post_date_gmt)) ?></p>
			<p class="autor"><?php the_author(); ?></p>
		</section>
	</header><!-- .entry-header -->
	<section class="content-post">
		<div class="entry-content">
			<?= the_content() ?>
		</div>
	</section>
	<footer>
		<?php
		$category_list = get_the_category($getID);
		if ( $category_list ) {
			echo '<div class="category"><ul>';
			foreach ($category_list as $cl) {
				echo '<li>' . $cl->cat_name . '</li>';
			}
			echo '</ul></div>';
		}
		?>
		<?php
		$tags_list = wp_get_post_tags($getID);
		if ( $tags_list ) {
			echo '<div class="tags"><ul>';
			foreach ($tags_list as $tl) {
				echo '<li>#' . $tl->name . '</li>';
			}
			echo '</ul></div>';
		}
		?>
	</footer>
</article>

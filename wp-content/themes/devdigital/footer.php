<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _mbbasetheme
 */
?>
<?php
if ( !is_front_page() ) {
	?>
	</div>
	</main>
<?php } ?>
<footer id="footer">
	<svg width="100%" height="100%" viewBox="0 0 400 300" preserveAspectRatio="none">
		<defs>
			<filter id="filter-footer">
				<feGaussianBlur in="SourceAlpha" stdDeviation="3.5" />
				<feOffset dx="0" dy="0" result="offsetblur" />
				<feFlood flood-color="rgba(0,0,0,0.2)" />
				<feComposite in2="offsetblur" operator="in" />
				<feMerge>
					<feMergeNode />
					<feMergeNode in="SourceGraphic" />
				</feMerge>
			</filter>
		</defs>
		<path d="M 0,70 L 0,300 400,300 400,30 330,5 0,70 Z" vector-effect="non-scaling-stroke"
		      filter="url(#filter-footer)"></path>
	</svg>
	<div class="container">
		<?php
		$footer_social = get_field('footer_social_link', 'option');
		if($footer_social){
		?>
		<ul class="social-link"><?php	foreach($footer_social as $fs){	?><li><a href="<?= $fs['link'] ?>" target="_blank"><?= $fs['social_svg'] ?></a></li><?php } ?></ul>
		<?php } ?>
		<?php
		$footer_telephone = get_field('footer_telephone_list', 'option');
		if($footer_telephone){
		?>
		<ul class="telephone-footer">
			<?php foreach($footer_telephone as $ft){?><li><a href="tel:<?= telNumber($ft['telephone']) ?>"><?=
					$ft['telephone'] ?></a></li><?php } ?>
		</ul>
		<?php } ?>
		<p class="sitemap"><a href="#"><?= get_field('sitemap','option')?></a></p>
		<p class="copyright">2013-<?= date('Y') ?>. <?= get_field('copyright','option')?></p>
	</div>
</footer>

<div class="callbackForm">

	<div class="form">
		<svg width="100%" height="100%" id="ContactFormSVG" viewBox="0 0 300 700" preserveAspectRatio="none">
			<defs>
				<filter id="filter-contact-form">
					<feGaussianBlur in="SourceAlpha" stdDeviation="3"></feGaussianBlur>
					<feOffset dx="0" dy="0" result="offsetblur"></feOffset>
					<feFlood flood-color="rgba(0,0,0,0.4)"></feFlood>
					<feComposite in2="offsetblur" operator="in"></feComposite>
					<feMerge>
						<feMergeNode></feMergeNode>
						<feMergeNode in="SourceGraphic"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<polygon points="306,710 306,-10 5,335" stroke="purple" fill="white" stroke-width="0" vector-effect="non-scaling-stroke" filter="url(#filter-contact-form)"></polygon>
			<g transform="translate(25,320)" class="openContactForm">
				<image width="27" height="25" class="contact-form-image mail"
				       xmlns:xlink="http://www.w3.org/1999/xlink"
				       xlink:href="/wp-content/themes/devdigital/assets/images/maill.svg">
				</image>
			</g>

		</svg>

		<div class="form-wrap">
			<div class="hidden mobile-label">
				<div id="ContactFormSVGMobile"></div>
			</div>
			<?php
			// ВЫВОД ФОРМЫ ОБРАТНОЙ СВЯЗИ
			languageFeedbackForm();
			?>
		</div>

	</div>
</div>
<?php include_once("analyticstracking.php") ?>
<?php wp_footer(); ?>
</body>
</html>

<?php
/**
 * Helper functions for use in other areas of the theme
 *
 * @package _mbbasetheme
 */
/**
 * Add capabilities for a custom post type
 *
 * @return void
 */
function mb_add_capabilities($posttype)
{
	// gets the author role
	$role = get_role('administrator');
	// adds all capabilities for a given post type to the administrator role
	$role->add_cap('edit_' . $posttype . 's');
	$role->add_cap('edit_others_' . $posttype . 's');
	$role->add_cap('publish_' . $posttype . 's');
	$role->add_cap('read_private_' . $posttype . 's');
	$role->add_cap('delete_' . $posttype . 's');
	$role->add_cap('delete_private_' . $posttype . 's');
	$role->add_cap('delete_published_' . $posttype . 's');
	$role->add_cap('delete_others_' . $posttype . 's');
	$role->add_cap('edit_private_' . $posttype . 's');
	$role->add_cap('edit_published_' . $posttype . 's');
}

function languageFeedbackForm()
{
	$current_language = qtranxf_getLanguage();
	switch ( $current_language ) {
		case 'ru':
//			echo '<a href="#" class="feedback-form"><span>Форма обратной связи</span></a><div class="feedback-form-wrap">';
			echo '<h2>Свяжитесь с нами</h2>';
			echo do_shortcode('[contact-form-7 id="128"]');
			break;
		default:
//			echo '<a href="#" class="feedback-form"><span>Feedback form</span></a><div class="feedback-form-wrap">';
			echo '<h2>Feedback form</h2>';
			echo do_shortcode('[contact-form-7 id="129" title="Feedback"]');
	}
	echo '</div>';
}


function languageWeSymbol(){
	$current_language = qtranxf_getLanguage();
	switch ( $current_language ) {
		case 'ru':
			$symbol = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
			break;
		case 'en':
			$symbol = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
			break;
	}
	return $symbol;
}
<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/wp-load.php";
ini_set('display_errors', 1);
//error_reporting('E_ALL');
define('SHORTINIT', 'faster');
require($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');

$category = $_POST['cat'];


$args = array(
	'post_type'      => 'projects',
	'posts_per_page' => -1
);
if($category != 'all'){
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'project_category',
			'field'    => 'slug',
			'terms'    => $category
		)
	);
}
$query = new WP_Query($args);
// Цикл
$result ='';
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

		ob_start();
		get_template_part('content', 'projects');
		$result .= ob_get_clean();
	}
}
wp_reset_postdata();


echo json_encode($result);
exit; // - обязательно
?>
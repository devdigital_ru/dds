<div class="inner-page-title">
	<svg width="100%" height="100%" id="mainTitle" viewBox="0 0 600 305" preserveAspectRatio="xMinYMin meet">
		<?php
		$symbol_start = 300;
		$symbol_lenght = 3.75;
		$title_lenght = strlen(get_the_title());
		$left = $symbol_start - ($title_lenght * $symbol_lenght);
		if ( is_archive() || is_search() || is_404() )
		$left = '270';
		if($left < 180) $left = 180;
		?>
		<g transform="translate(<?= $left ?>,0)" class="textWrap">
			<g transform="translate(-30,0)">
				<foreignObject width="<?= is_search() ? '80%' : '70%' ?>" height="33%" y="30%">
					<body xmlns="http://www.w3.org/1999/xhtml">
					<header class="page-title-inner">
						<hgroup>
							<div class="title-wrap">
								<?php
								$style = '';
								$shape = '';
								$shane_none_style = '';
								if($title_lenght > 50){
									$style=" style='font-size: 18px;padding-left: 18px;'";
									$shape = '<span class="shape"></span>';
									$shane_none_style= ' style="shape-outside: none; border: 0; display:
									inline-block; width: 0px; height: 10px; margin-left: -30px;"';
								}
								if(is_404()){
									echo '<h1'.$style.'>'.$shape.get_field('error_404', 'option').'</h1>';
								}elseif(is_search()){
									echo '<h1'.$style.'>'.$shape.get_the_title(111).'</h1>';
//								echo '<h1>'.get_field('result_search', 'option').': «'.$_GET['s'].'»</h1>';
								}elseif ( is_archive() ) {
									echo '<h1'.$style.'>'.$shape.get_the_title(111).'</h1>';
//									the_archive_title('<h1>', '</h1>');
								} else {
									?>
									<?= the_title('<h1'.$style.'>'.$shape, '</h1>') ?>
								<?php } ?>
								<div class="wrap-inner">
									<span class="shape"<?= $shane_none_style ?>></span>
									<?php if ( get_field('subtitle') ) { ?>
										<h2><?= get_field('subtitle') ?></h2><?php } ?>
									<?php if ( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
								</div>

								<?php
								if ( is_page('projects') ){
									$args = array(
										'type'         => 'projects',
										'child_of'     => 0,
										'parent'       => '',
										'orderby'      => 'name',
										'order'        => 'ASC',
										'hide_empty'   => 1,
										'hierarchical' => 0,
										'exclude'      => '',
										'include'      => '',
										'number'       => 0,
										'taxonomy'     => 'project_category',
										'pad_counts'   => false,
										// полный список параметров смотрите в описании функции http://wp-kama.ru/function/get_terms
									);
									$categories = get_categories( $args );
									if( $categories ){
										echo '<ul class="project-categories">';
										echo '<li data-item="all">Все проекты</li>';
										foreach( $categories as $cat ){
											// Данные в объекте $cat
											echo "<li data-item='$cat->slug'>$cat->name</li>";

											// $cat->term_id
											// $cat->name (Рубрика 1)
											// $cat->slug (rubrika-1)
											// $cat->term_group (0)
											// $cat->term_taxonomy_id (4)
											// $cat->taxonomy (category)
											// $cat->description (Текст описания)
											// $cat->parent (0)
											// $cat->count (14)
											// $cat->object_id (2743)
											// $cat->cat_ID (4)
											// $cat->category_count (14)
											// $cat->category_description (Текст описания)
											// $cat->cat_name (Рубрика 1)
											// $cat->category_nicename (rubrika-1)
											// $cat->category_parent (0)

										}
										echo '</ul>';
									}
								}
								?>

							</div>
						</hgroup>
					</header>
					</body>
				</foreignObject>
			</g>
			<g class="text_field" transform="translate(-36,115)">
			</g>
			<g>
				<line transform="rotate(45 60 123)" x1="0" y1="0" x2="0" y2="350" stroke-width="10"
				      stroke="rgb(255,255,255)"></line>
				<line transform="rotate(45 60 130)" x1="0" y1="0" x2="0" y2="350" stroke-width=".3"
				      stroke="rgb(0,0,0)"></line>
				<line transform="rotate(45 60 134)" x1="0" y1="0" x2="0" y2="80" stroke-width=".3"
				      stroke="rgb(0,0,0)"></line>
			</g>
			<g transform="translate(-85,185)" width="100" class="hover_group">
				<rect x="0" y="0" width="100" height="15" transform="skewX(-45)"></rect>
				<text x="15" y="8" font-size="8" text-anchor="start"
				      alignment-baseline="middle"><?= get_field('connect_with_us', 'option') ?></text>
			</g>
		</g>
	</svg>
	<div id="mobileTitle">
		<header class="page-title-inner-mobile">
			<hgroup>
				<div class="title-wrap">
					<h1><?= the_title() ?></h1>
					<div class="wrap-inner">
						<?php if ( get_field('subtitle') ) { ?><h2><?= get_field('subtitle') ?></h2><?php } ?>
						<?php if ( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
					</div>
				</div>
			</hgroup>
		</header>
	</div>
</div>
<script>
	var foreignDontWork = {
		"title": <?= json_encode(get_the_title()) ?>,
		"description": <?= json_encode(strip_tags(str_replace("<br />", "***", str_replace("\n", "", get_field('subtitle'))))) ?>
	}
</script>
<main role="main" id="content">
	<div class="container">
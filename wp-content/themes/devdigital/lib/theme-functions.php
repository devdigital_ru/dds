<?php
/**
 * _mbbasetheme theme functions definted in /lib/init.php
 *
 * @package _mbbasetheme
 */
/**
 * Register Widget Areas
 */
function mb_widgets_init()
{
	// Main Sidebar
	register_sidebar(array(
		'name'          => __('Sidebar', '_mbbasetheme'),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	));
}

/**
 * Remove Dashboard Meta Boxes
 */
function mb_remove_dashboard_widgets()
{
	global $wp_meta_boxes;
	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}

/**
 * Change Admin Menu Order
 */
function mb_custom_menu_order($menu_ord)
{
	if ( !$menu_ord ) return true;
	return array(
		// 'index.php', // Dashboard
		// 'separator1', // First separator
		// 'edit.php?post_type=page', // Pages
		// 'edit.php', // Posts
		// 'upload.php', // Media
		// 'gf_edit_forms', // Gravity Forms
		// 'genesis', // Genesis
		// 'edit-comments.php', // Comments
		// 'separator2', // Second separator
		// 'themes.php', // Appearance
		// 'plugins.php', // Plugins
		// 'users.php', // Users
		// 'tools.php', // Tools
		// 'options-general.php', // Settings
		// 'separator-last', // Last separator
	);
}

/**
 * Hide Admin Areas that are not used
 */
function mb_remove_menu_pages()
{
	// remove_menu_page( 'link-manager.php' );
}

/**
 * Remove default link for images
 */
function mb_imagelink_setup()
{
	$image_set = get_option('image_default_link_type');
	if ( $image_set !== 'none' ) {
		update_option('image_default_link_type', 'none');
	}
}

/**
 * Enqueue scripts
 */
function mb_scripts()
{
	wp_enqueue_style('_mbbasetheme-style', get_stylesheet_uri());
	if ( is_singular() && comments_open() && get_option('thread_comments') ) {
		wp_enqueue_script('comment-reply');
	}
//	if ( !is_admin() ) {
//		wp_enqueue_script( 'jquery' );
//		wp_enqueue_script( 'customplugins', get_template_directory_uri() . '/assets/js/plugins.min.js', array('jquery'), NULL, true );
//		wp_enqueue_script( 'customscripts', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), NULL, true );
//	}
}

/**
 * Remove Query Strings From Static Resources
 */
function mb_remove_script_version($src)
{
	$parts = explode('?ver', $src);
	return $parts[0];
}

/**
 * Remove Read More Jump
 */
function mb_remove_more_jump_link($link)
{
	$offset = strpos($link, '#more-');
	if ( $offset ) {
		$end = strpos($link, '"', $offset);
	}
	if ( $end ) {
		$link = substr_replace($link, '', $offset, $end - $offset);
	}
	return $link;
}

// КАСТОМНАЯ ТАКСОНОМИЯ
function add_custom_taxonomies()
{
	// Add new "Locations" taxonomy to Posts
	register_taxonomy('project_category', 'projects', array(
		// Hierarchical taxonomy (like categories)
		'hierarchical' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels'       => array(
			'name'              => _x('Project category', 'taxonomy general name'),
			'singular_name'     => _x('project_category', 'taxonomy singular name'),
			'search_items'      => __('Search project category'),
			'all_items'         => __('All project category'),
			'parent_item'       => __('Parent project category'),
			'parent_item_colon' => __('Parent project category:'),
			'edit_item'         => __('Edit project category'),
			'update_item'       => __('Update project category'),
			'add_new_item'      => __('Add New project category'),
			'new_item_name'     => __('New Location Name'),
			'menu_name'         => __('Категории проектов'),
		),
		// Control the slugs used for this taxonomy
		'rewrite'      => array(
			'slug'         => 'project_category', // This controls the base slug that will display before each term
			'with_front'   => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	));
}

add_action('init', 'add_custom_taxonomies', 0);
// СВОИ ТИПЫ ЗАПИСЕЙ
add_action('init', 'create_custom_post');
function create_custom_post()
{
	// проекты
	$labels = array(
		'name'               => 'Проекты',
		'singular_name'      => 'Проекты',
		'add_new'            => 'Добавить новый проект',
		'add_new_item'       => 'Добавить новый проект',
		'edit'               => 'Редактировать проект',
		'edit_item'          => 'Редактировать проект',
		'new_item'           => 'Новый проект',
		'view'               => 'Просмотр',
		'view_item'          => 'Просмотр проекта',
		'search_items'       => 'Искать проект',
		'not_found'          => 'Не найдено ни одного закона',
		'not_found_in_trash' => 'Законов в корзине не найдено',
		'parent'             => 'Родительский проект',
		'parent_item_colon'  => 'Текст родительского элемента',
		'all_items'          => 'Все проекты'
	);
	$supports = array(
		'title',
		'editor'
	);
	$rewrite = array(
		'enabled'    => 1,
		'custom'     => 'normal',
		'slug'       => '',
		'with_front' => true,
		'feeds'      => true,
		'pages'      => true
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => $supports,
		'rewrite'             => $rewrite,
		'slug'                => 'projects',
		'label'               => 'projects',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_admin_bar'   => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-status',
		'hierarchical'        => false,
		'taxonomies'          => array(
			'location',
			'post_tag'
		),
		'has_archive'         => false,
		'can_export'          => true,
		'query_var'           => true
	);
	register_post_type('projects', $args);
	flush_rewrite_rules();
// абракадабра
	$labels = array(
		'name'               => 'Абракадабра',
		'singular_name'      => 'Абракадабра',
		'add_new'            => 'Добавить',
		'add_new_item'       => 'Добавить',
		'edit'               => 'Редактировать',
		'edit_item'          => 'Редактировать',
		'new_item'           => 'Новый',
		'view'               => 'Просмотр',
		'view_item'          => 'Просмотр',
		'search_items'       => 'Искать',
		'not_found'          => 'Не найдено ни одной записи',
		'not_found_in_trash' => 'Записей в корзине не найдено',
		'parent'             => 'Родительская запись',
		'parent_item_colon'  => 'Текст родительского элемента',
		'all_items'          => 'Все позиции'
	);
	$supports = array(
		'title',
		'editor'
	);
	$rewrite = array(
		'enabled'    => 1,
		'custom'     => 'normal',
		'slug'       => '',
		'with_front' => true,
		'feeds'      => true,
		'pages'      => true
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => $supports,
		'rewrite'             => $rewrite,
		'slug'                => 'abrakadabra',
		'label'               => 'abrakadabra',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_admin_bar'   => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-format-status',
		'hierarchical'        => false,
//		'taxonomies' => array(
//			'category',
//			'post_tag'
//		),
		'has_archive'         => false,
		'can_export'          => true,
		'query_var'           => true
	);
	register_post_type('abrakadabra', $args);
	flush_rewrite_rules();
// услуги
	$labels = array(
		'name'               => 'Услуги',
		'singular_name'      => 'Услуги',
		'add_new'            => 'Добавить новую услугу',
		'add_new_item'       => 'Добавить новую услугу',
		'edit'               => 'Редактировать',
		'edit_item'          => 'Редактировать',
		'new_item'           => 'Новая услуга',
		'view'               => 'Просмотр',
		'view_item'          => 'Просмотр',
		'search_items'       => 'Искать',
		'not_found'          => 'Не найдено ни одной записи',
		'not_found_in_trash' => 'Записей в корзине не найдено',
		'parent'             => 'Родительская услуга',
		'parent_item_colon'  => 'Текст родительского элемента',
		'all_items'          => 'Все позиции'
	);
	$supports = array(
		'title',
		'editor'
	);
	$rewrite = array(
		'enabled'    => 1,
		'custom'     => 'normal',
		'slug'       => '',
		'with_front' => false,
		'feeds'      => true,
		'pages'      => true
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => $supports,
		'rewrite'             => $rewrite,
		'slug'                => 'services',
		'label'               => 'services',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_admin_bar'   => true,
		'show_in_menu'        => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-format-status',
		'hierarchical'        => true,
//		'taxonomies' => array(
//			'category',
//			'post_tag'
//		),
		'has_archive'         => false,
		'can_export'          => true,
		'query_var'           => true,
		'supports'            => array('title', 'editor', 'page-attributes'),
	);
	register_post_type('services', $args);
	flush_rewrite_rules();
	// акции
	$labels = array(
		'name'               => 'Акции',
		'singular_name'      => 'Акция',
		'add_new'            => 'Добавить новую акцию',
		'add_new_item'       => 'Добавить новую акцию',
		'edit'               => 'Редактировать',
		'edit_item'          => 'Редактировать',
		'new_item'           => 'Новая услуга',
		'view'               => 'Просмотр',
		'view_item'          => 'Просмотр',
		'search_items'       => 'Искать',
		'not_found'          => 'Не найдено ни одной записи',
		'not_found_in_trash' => 'Записей в корзине не найдено',
		'parent'             => 'Родительская запись',
		'parent_item_colon'  => 'Текст родительского элемента',
		'all_items'          => 'Все позиции'
	);
	$supports = array(
		'title',
		'editor'
	);
	$rewrite = array(
		'enabled'    => 1,
		'custom'     => 'normal',
		'slug'       => '',
		'with_front' => true,
		'feeds'      => true,
		'pages'      => true
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => $supports,
		'rewrite'             => $rewrite,
		'slug'                => 'sale',
		'label'               => 'sale',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_admin_bar'   => true,
		'show_in_menu'        => true,
		'menu_position'       => 7,
		'menu_icon'           => 'dashicons-format-status',
		'hierarchical'        => false,
//		'taxonomies' => array(
//			'category',
//			'post_tag'
//		),
		'has_archive'         => false,
		'can_export'          => true,
		'query_var'           => true
	);
	register_post_type('sale', $args);
	flush_rewrite_rules();
	// отзывы
	$labels = array(
		'name'               => 'Отзывы',
		'singular_name'      => 'Отзыв',
		'add_new'            => 'Добавить новый отзыв',
		'add_new_item'       => 'Добавить новый отзыв',
		'edit'               => 'Редактировать',
		'edit_item'          => 'Редактировать',
		'new_item'           => 'Новая услуга',
		'view'               => 'Просмотр',
		'view_item'          => 'Просмотр',
		'search_items'       => 'Искать',
		'not_found'          => 'Не найдено ни одной записи',
		'not_found_in_trash' => 'Записей в корзине не найдено',
		'parent'             => 'Родительская запись',
		'parent_item_colon'  => 'Текст родительского элемента',
		'all_items'          => 'Все позиции'
	);
	$supports = array(
		'title',
		'editor'
	);
	$rewrite = array(
		'enabled'    => 1,
		'custom'     => 'normal',
		'slug'       => '',
		'with_front' => true,
		'feeds'      => true,
		'pages'      => true
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => $supports,
		'rewrite'             => $rewrite,
		'slug'                => 'reviews',
		'label'               => 'reviews',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_admin_bar'   => true,
		'show_in_menu'        => true,
		'menu_position'       => 7,
		'menu_icon'           => 'dashicons-format-status',
		'hierarchical'        => false,
//		'taxonomies' => array(
//			'category',
//			'post_tag'
//		),
		'has_archive'         => false,
		'can_export'          => true,
		'query_var'           => true
	);
	register_post_type('reviews', $args);
	flush_rewrite_rules();
}

//
//function themes_fonts()
//{
//	wp_enqueue_style('my-font-jannon10', get_stylesheet_directory_uri() . '/assets/css/fonts/jannon10/styles.css');
//	wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,cyrillic-ext', false);
//}
// ПОДКЛЮЧАЕМ СТИЛИ
function themes_style()
{
	wp_enqueue_style('main-normalize', get_stylesheet_directory_uri() . '/assets/css/normalize.css', false);
	wp_enqueue_style('main-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css', false);
	wp_enqueue_style('main-font-style', get_stylesheet_directory_uri() . '/assets/css/fonts/style.css', false);
//	wp_enqueue_style('main-page', get_stylesheet_directory_uri() . '/assets/css/font-awesome.css', false);
//	wp_enqueue_style('main-scrollbar', get_stylesheet_directory_uri() . '/assets/css/optiscroll.css', false);
//	wp_enqueue_style('main-scrollbar', get_stylesheet_directory_uri() . '/assets/css/jquery.mCustomScrollbar.min.css', false);
	wp_enqueue_style('main-style', get_stylesheet_directory_uri() . '/assets/css/style.css', false);
	wp_enqueue_style('main-magnific-popup', get_stylesheet_directory_uri() . '/assets/css/magnific-popup.css', false);
	if ( is_front_page() ) {
		wp_enqueue_style('main-page', get_stylesheet_directory_uri() . '/assets/css/main.css', false);
	}
	if ( is_page('we') ) { // СТРАНИЦА МЫ
		wp_enqueue_style('we-page', get_stylesheet_directory_uri() . '/assets/css/we.css', false);
//		wp_enqueue_style('photoSwipe', get_stylesheet_directory_uri() . '/assets/css/photoswipe.css', false);
//		wp_enqueue_style('photoSwipe2', get_stylesheet_directory_uri() . '/assets/css/default-skin/default-skin.css',false);
		wp_enqueue_style('animate-css', get_stylesheet_directory_uri() . '/assets/css/animate.min.css', false);
	}
	if ( is_page('projects') ) {
		wp_enqueue_style('projects-page', get_stylesheet_directory_uri() . '/assets/css/projects-list.css', false);
	}
	if ( is_page('abrakadabra') ) {
		wp_enqueue_style('abrakadabra-page', get_stylesheet_directory_uri() . '/assets/css/abrakadabra.css', false);
	}
	if ( is_page('contacts') ) {
		wp_enqueue_style('abrakadabra-page', get_stylesheet_directory_uri() . '/assets/css/contacts.css', false);
	}
	if ( is_page('services') ) {
		wp_enqueue_style('abrakadabra-page', get_stylesheet_directory_uri() . '/assets/css/services.css', false);
	}
	if ( is_singular('services') ) {
		wp_enqueue_style('animate-css', get_stylesheet_directory_uri() . '/assets/css/animate.min.css', false);
		wp_enqueue_style('slick', get_stylesheet_directory_uri() . '/assets/css/slick.css', false);
		wp_enqueue_style('slick-theme', get_stylesheet_directory_uri() . '/assets/css/slick-theme.css', false);
		wp_enqueue_style('services-page', get_stylesheet_directory_uri() . '/assets/css/service-inner.css', false);
	}
	if ( is_page('sale') ) {
		wp_enqueue_style('sale-page', get_stylesheet_directory_uri() . '/assets/css/sale.css', false);
	}
	if ( is_singular('sale') ) {
		wp_enqueue_style('sale-page', get_stylesheet_directory_uri() . '/assets/css/sale-inner.css', false);
	}
	if ( is_page('sitemap') ) {
		wp_enqueue_style('sitemap-page', get_stylesheet_directory_uri() . '/assets/css/sitemap.css', false);
	}
	if ( is_404() ) {
		wp_enqueue_style('404-page', get_stylesheet_directory_uri() . '/assets/css/404.css', false);
	}
	if ( is_page('blog') || is_singular('post') || is_archive() || is_search() ) {
		wp_enqueue_style('blog-page', get_stylesheet_directory_uri() . '/assets/css/blog.css', false);
	}
	if ( is_singular('post') ) {
		wp_enqueue_style('rr', get_stylesheet_directory_uri() . '/assets/css/rr_light.css', false);
	}
	if ( is_singular('projects') ) {
		wp_enqueue_style('projects-page-inner', get_stylesheet_directory_uri() . '/assets/css/project.css', false);
//		wp_enqueue_style('projects-page-inner-covering-foundation', get_stylesheet_directory_uri() . '/assets/js/twentytwenty/css/foundation.css', false);
		wp_enqueue_style('projects-page-inner-covering-twentytwenty', get_stylesheet_directory_uri() . '/assets/js/twentytwenty/css/twentytwenty.css', false);
//		wp_enqueue_style('slick', get_stylesheet_directory_uri() . '/assets/css/slick.css',false);
//		wp_enqueue_style('slick-theme', get_stylesheet_directory_uri() . '/assets/css/slick-theme.css',
//			false);
	}
}

// Async load
function add_asyncdefer_attribute($tag, $handle)
{
	$param = '';
	if ( strpos($handle, 'async') !== false ) $param = 'async ';
	if ( strpos($handle, 'defer') !== false ) $param .= 'defer ';
	if ( $param )
		return str_replace('<script ', '<script ' . $param, $tag);
	else
		return $tag;
}

add_filter('script_loader_tag', 'add_asyncdefer_attribute', 10, 2);
add_action('wp_enqueue_scripts', 'themes_style');
//// ПОДКЛЮЧАЕМ СКРИПТЫ
function themes_script()
{
	wp_deregister_script('jquery');
	wp_deregister_script('contact-form-7-css');
	wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.js', array(), '1.0.0', true);
	wp_enqueue_script('d3-js', 'https://d3js.org/d3.v4.min.js', array(), '1.0.0', true);
	wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/assets/js/lib/bootstrap.min.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('polyfill-js-async-defer', get_stylesheet_directory_uri() . '/assets/js/lib/shapes-polyfill.min.js', 'jquery', '', true);
	wp_enqueue_script('enquire', get_stylesheet_directory_uri() . '/assets/js/lib/enquire.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('com', get_stylesheet_directory_uri() . '/assets/js/lib/com.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0.0', true);
	if ( is_front_page() ) {
		wp_enqueue_script('threejs', get_stylesheet_directory_uri() . '/assets/js/threejs/three.min.js', array(), '1.0.0', true);
		wp_enqueue_script('Projector', get_stylesheet_directory_uri() . '/assets/js/threejs/Projector.js', array(), '1.0.0', true);
		wp_enqueue_script('CanvasRenderer', get_stylesheet_directory_uri() . '/assets/js/threejs/CanvasRenderer.js', array(), '1.0.0', true);
//		wp_enqueue_script('stats', get_stylesheet_directory_uri() . '/assets/js/threejs/stats.min.js', array(), '1.0.0', true);
		wp_enqueue_script('main-main', get_stylesheet_directory_uri() . '/assets/js/main/main.js', array('jquery'), '1.0.0', true);
	}
	if ( is_page('we') ) {
		wp_enqueue_script('popup', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.magnific-popup.min.js', array('jquery'), '1.0.0', true);
//		wp_enqueue_script('photoSwipe1', get_stylesheet_directory_uri() . '/assets/js/lib/photoswipe.min.js', array(), '1.0.0',	true);
//		wp_enqueue_script('photoSwipe2', get_stylesheet_directory_uri() . '/assets/js/lib/photoswipe-ui-default.min.js', array(), '1.0.0', true);
		wp_enqueue_script('animatedModal-async-defer', get_stylesheet_directory_uri() . '/assets/js/lib/animatedModal.min.js', array(), '1.0.0', true);
//		wp_enqueue_script('naoTooltips', get_stylesheet_directory_uri() . '/assets/js/lib/naoTooltips.js',array(), '1.0.0', true);
		wp_enqueue_script('we-main-async-defer', get_stylesheet_directory_uri() . '/assets/js/we/we.js', array('jquery'), '1.0.0', true);
	}
	if ( is_page('projects') ) {
		wp_enqueue_script('project-main', get_stylesheet_directory_uri() . '/assets/js/projects/project-list.js', array('jquery'), '1.0.0', true);
	}
	if ( is_singular('projects') ) {
		wp_enqueue_script('coveringBad-event-async-defer', get_stylesheet_directory_uri() . '/assets/js/twentytwenty/js/jquery.event.move.js', array(), '1.0.0', true);
		wp_enqueue_script('coveringBad-twenty-async-defer', get_stylesheet_directory_uri() . '/assets/js/twentytwenty/js/jquery.twentytwenty.js', array(), '1.0.0', true);
//		wp_enqueue_script('scroll-project-async-defer', get_stylesheet_directory_uri() . '/assets/js/lib/jquery.overscroll.min.js', array(), '1.0.0', true);
		wp_enqueue_script('scroll-project', get_stylesheet_directory_uri() . '/assets/js/lib/dragscroll.js', array(), '1.0.0', true);
		wp_enqueue_script('project-main-async-defer', get_stylesheet_directory_uri() . '/assets/js/projects/project.js', array('jquery'), '1.0.0', true);
	}
	if ( is_page('contacts') ) {
		wp_enqueue_script('jquery-ui', get_stylesheet_directory_uri() . '/assets/js/lib/jquery-ui.min.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyBNOYJlUz83nzrIpUrBcOebS3GLuknCSVA&v=3.exp', array(), '1.0.0', true);
		wp_enqueue_script('contacts-main', get_stylesheet_directory_uri() . '/assets/js/contacts/contacts.js', array('jquery'), '1.0.0', true);
	}
	if ( is_singular('post') ) {
		wp_enqueue_script('readremaining', get_stylesheet_directory_uri() . '/assets/js/lib/readremaining.jquery.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('cookie', get_stylesheet_directory_uri() . '/assets/js/lib/cookie.js', array(), '1.0.0', true);
	}
	if ( is_page('blog') || is_singular('post') ) {
		wp_enqueue_script('blog-main', get_stylesheet_directory_uri() . '/assets/js/blog/blog.js', array('jquery'), '1.0.0', true);
	}
	if ( is_page('sale') ) {
		wp_enqueue_script('sale-main', get_stylesheet_directory_uri() . '/assets/js/sale/sale.js', array('jquery'), '1.0.0', true);
	}
	if ( is_singular('services') ) {
		wp_enqueue_script('animatedModal', get_stylesheet_directory_uri() . '/assets/js/lib/animatedModal.min.js', array(), '1.0.0', true);
		wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/assets/js/lib/slick.min.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('common-slick', get_stylesheet_directory_uri() . '/assets/js/services/common-slick.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('sale-main', get_stylesheet_directory_uri() . '/assets/js/services/inner.js', array('jquery'), '1.0.0', true);
	}
}

add_action('wp_enqueue_scripts', 'themes_script');
//// брезать все, кроме цифр
//function number_telephone($str)
//{
//	echo preg_replace('/[^0-9]/', '', $str);
//}
//
// переименовывваем записи в блок
function change_post_menu_label()
{
	global $menu;
	global $submenu;
	$menu[5][0] = __('Блог', 'i18n_context');
	$submenu['edit.php'][5][0] = __('Блог', 'i18n_context');
	$submenu['edit.php'][10][0] = __('Добавить запись блог', 'i18n_context');
	$submenu['edit.php'][15][0] = __('Категории', 'i18n_context');
	$submenu['edit.php'][16][0] = __('Теги', 'i18n_context');
	echo '';
}

add_action('admin_menu', 'change_post_menu_label');
function positionWordArray($word, $widthFilword, $heightFilword)
{
	/*
	 * Позиция слова:
	 * 0 - горизонтально
	 * 1 - вертикально
	 */
	$wordLenght = iconv_strlen($word);
	/*
	 * Проверяем на длину слово, куда оно может поместиться
	 * Если есть выбор, делаем рандом
	 */
	if ( ($wordLenght < $widthFilword) && ($wordLenght < $heightFilword) ) {
		$positionWord = rand(1, 2);
	} else {
		if ( $wordLenght < $widthFilword ) {
			$positionWord = 1;
		} else {
			$positionWord = 2;
		}
	}
//	$positionWord = 1;
	/*
 * находим позицию слову
 */
	$pos['align'] = $positionWord;
	if ( $positionWord == 1 ) {
		/*
		 * ГОРИЗОНТАЛЬНОЕ СЛОВО
		 */
		$pos['column'] = rand(1, $widthFilword - $wordLenght) - 1;
		$pos['line'] = rand(1, $heightFilword) - 1;
		$pos['newColumn'] = $pos['column'] + $wordLenght;
		$pos['newLine'] = $pos['line'] + 1;
	} else {
		/*
		 * ВЕРТИКАЛЬНОЕ СЛОВО
		 */
		$pos['column'] = rand(1, $widthFilword) - 1;
		$pos['line'] = rand(1, $heightFilword - $wordLenght) - 1;
		$pos['newColumn'] = $pos['column'] + 1;
		$pos['newLine'] = $pos['line'] + $wordLenght;
	}
	$pos['wordLenght'] = $wordLenght;
	return $pos;
}

function replaceYMD($val, $lang)
{
//	{year} {mounth} {day}
	$first_time = date(strtotime("1 Mar 2013 12:00"));
	$now_time = time();
	$days = date_diff(
		date_create('@' . $now_time),
		date_create('@' . $first_time)
	);
	$years_text = '';
	$mounth_text = '';
	switch ( $days->y ) {
		case 1:
			$years_text = ' ' . get_field('we_year1', 'option');
			break;
		case 2:
			$years_text = ' ' . get_field('we_year2', 'option');
			break;
		case 3:
			$years_text = ' ' . get_field('we_year2', 'option');
			break;
		case 4:
			$years_text = ' ' . get_field('we_year2', 'option');
			break;
		default:
			$years_text = ' ' . get_field('we_year3', 'option');
	}
	switch ( $days->m ) {
		case 1:
			$mounth_text = ' ' . get_field('we_mounth1', 'option');
			break;
		case 2:
			$mounth_text = ' ' . get_field('we_mounth2', 'option');
			break;
		case 3:
			$mounth_text = ' ' . get_field('we_mounth2', 'option');
			break;
		case 4:
			$mounth_text = ' ' . get_field('we_mounth2', 'option');
			break;
		default:
			$mounth_text = ' ' . get_field('we_mounth3', 'option');
	}
	if ( $days->y[strlen($days->y) - 1] == '1' ) {
		$days_text = ' ' . get_field('we_day1', 'option');
	} elseif ( in_array($days->y[strlen($days->y) - 1], array(2, 3, 4)) ) {
		$days_text = ' ' . get_field('we_day2', 'option');
	} else {
		$days_text = ' ' . get_field('we_day3', 'option');
	}
	$val = str_replace("{year}", $days->y . $years_text, $val);
	$val = str_replace("{mounth}", $days->m . $mounth_text, $val);
	$val = str_replace("{day}", $days->d . $days_text, $val);
	return $val;
}

// УДАЛЕНИЕ И ДОБАВЛЕНИЯ КЛАССА ПУНКТАМ МЕНЮ
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
function my_css_attributes_filter($var)
{
	if ( is_array($var) )
		array_push($var, 'nav-item');
	return is_array($var) ? array_intersect($var, array('current-menu-item', 'nav-item')) : '';
}

// СМЕНА ФОРМАТА ДАТЫ У КОММЕНТАРИЯ
add_filter('get_comment_date', 'wpsites_change_comment_date_format');
function wpsites_change_comment_date_format($d)
{
	$d = date("d.m.Y");
	return $d;
}

register_sidebar(array(
	'name'          => 'Внутренняя блога',
	'id'            => 'secondary-widget-area',
	'before_widget' => '<aside id="%1$s" class="widget-container %2$s" role="complementary">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h1 class="widget-title">',
	'after_title'   => '</h1>',
));
// КАСТОМНЫЙ ВИДЖЕТ АВТОРА
// Creating the widget
class wpb_widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(
// Base ID of your widget
			'wpb_author',
// Widget name will appear in UI
			__('Autor post', 'wpb_widget_author'),
// Widget description
			array('description' => __('Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain'),)
		);
	}

// Creating widget front-end
// This is where the action happens
	public function widget($args, $instance)
	{
		$title = apply_filters('widget_title', $instance['title']);
// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( !empty($title) )
			echo $args['before_title'] . $title . $args['after_title'];
		$author_ID = get_the_author_meta('ID');
		$author_NAME = get_the_author_meta('display_name');
		$author_DESCRIPTION = get_the_author_meta('description');
		echo '<div class="author-post">';
		echo '<div class="author-image">';
		echo __(get_avatar($author_ID, 250), 'wpb_widget_domain');
		echo '</div>';
		echo '<div class="author-name">';
		echo __($author_NAME, 'wpb_widget_domain');
		echo '</div>';
		echo '<div class="author-description">';
		echo __($author_DESCRIPTION, 'wpb_widget_domain');
		echo '</div>';
		echo '</div>';
		echo $args['after_widget'];
	}

// Widget Backend
	public function form($instance)
	{
		if ( isset($instance['title']) ) {
			$title = $instance['title'];
		} else {
			$title = __('New title', 'wpb_widget_domain');
		}
// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
			       name="<?php echo $this->get_field_name('title'); ?>" type="text"
			       value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php
	}

// Updating widget replacing old instances with new
	public function update($new_instance, $old_instance)
	{
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
} // Class wpb_widget ends here
// Register and load the widget
function wpb_load_widget()
{
	register_widget('wpb_widget');
}

add_action('widgets_init', 'wpb_load_widget');
// КАСТОМНЫЙ ВИДЖЕТ категорий
// Creating the widget
class wcategory_widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct(
// Base ID of your widget
			'wpb_category',
// Widget name will appear in UI
			__('Category hierarhical list', 'wpb_widget_category'),
// Widget description
			array('description' => __('Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain'),)
		);
	}

// Creating widget front-end
// This is where the action happens
	public function widget($args, $instance)
	{
		$title = apply_filters('widget_title', $instance['title']);
// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( !empty($title) )
			echo $args['before_title'] . $title . $args['after_title'];
		$args = array(
			'show_option_all'     => false,
			'show_option_none'    => false,
			'orderby'             => 'name',
			'order'               => 'ASC',
			'show_last_update'    => 0,
			'style'               => 'list',
			'show_count'          => 0,
			'hide_empty'          => 1,
			'use_desc_for_title'  => 1,
			'child_of'            => 0,
			'feed'                => '',
			'feed_type'           => '',
			'feed_image'          => '',
			'exclude'             => '',
			'exclude_tree'        => '',
			'include'             => '',
			'hierarchical'        => true,
			'title_li'            => false,
			'number'              => null,
			'echo'                => 1,
			'depth'               => 0,
			'current_category'    => 0,
			'pad_counts'          => 0,
			'taxonomy'            => 'category',
			'walker'              => 'Walker_Category',
			'hide_title_if_empty' => false,
			'separator'           => '<br />',
		);
		echo '<ul class="list-category">';
		wp_list_categories($args);
		echo '</ul>';
		echo $args['after_widget'];
		echo '</aside>';
	}

// Widget Backend
	public function form($instance)
	{
		if ( isset($instance['title']) ) {
			$title = $instance['title'];
		} else {
			$title = __('New title', 'wpb_widget_domain');
		}
// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
			       name="<?php echo $this->get_field_name('title'); ?>" type="text"
			       value="<?php echo esc_attr($title); ?>" />
		</p>
		<?php
	}

// Updating widget replacing old instances with new
	public function update($new_instance, $old_instance)
	{
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
} // Class wpb_widget ends here
// Register and load the widget
function wpb_category_widget()
{
	register_widget('wcategory_widget');
}

add_action('widgets_init', 'wpb_category_widget');
function telNumber($str)
{
	return preg_replace('/[^0-9]/', '', $str);
}

function dimox_breadcrumbs()
{
	/* === ОПЦИИ === */
	$text['home'] = get_field('main', 'option'); // текст ссылки "Главная"
	$text['category'] = '%s'; // текст для страницы рубрики
	$text['search'] = get_field('result_search', 'option') . ' «%s»'; // текст для страницы с результатами поиска
	$text['tag'] = get_field('post_tag', 'option') . ' «%s»'; // текст для страницы тега
	$text['author'] = get_field('post_autor', 'option') . ' %s'; // текст для страницы автора
	$text['404'] = get_field('error_404', 'option'); // текст для страницы 404
	$text['page'] = get_field('page', 'option') . ' %s'; // текст 'Страница N'
	$text['cpage'] = get_field('page_comments', 'option') . ' %s'; // текст 'Страница комментариев N'
	$wrap_before = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
	$wrap_after = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
	$sep = '›'; // разделитель между "крошками"
	$sep_before = '<span class="sep">'; // тег перед разделителем
	$sep_after = '</span>'; // тег после разделителя
	$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
	$show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
	$show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
	$before = '<span class="current">'; // тег перед текущей "крошкой"
	$after = '</span>'; // тег после текущей "крошки"
	/* === КОНЕЦ ОПЦИЙ === */
	global $post;
	$home_url = home_url('/');
	$link_before = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link_after = '</span>';
	$link_attr = ' itemprop="item"';
	$link_in_before = '<span itemprop="name">';
	$link_in_after = '</span>';
	$link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
	$frontpage_id = get_option('page_on_front');
	$parent_id = $post->post_parent;
	$sep = ' ' . $sep_before . $sep . $sep_after . ' ';
	$home_link = $link_before . '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;
	if ( is_home() || is_front_page() ) {
		if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;
	} else {
		echo $wrap_before;
		if ( $show_home_link ) echo $home_link;
		if ( is_category() ) {
			$cat = get_category(get_query_var('cat'), false);
			if ( $cat->parent != 0 ) {
				$cats = get_category_parents($cat->parent, true, $sep);
				$cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
				if ( $show_home_link ) echo $sep;
				echo $cats;
			}
			if ( get_query_var('paged') ) {
				$cat = $cat->cat_ID;
				echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ( $show_current ) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
			}
		} elseif ( is_search() ) {
			if ( have_posts() ) {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf($text['search'], get_search_query()) . $after;
			} else {
				if ( $show_home_link ) echo $sep;
				echo $before . sprintf($text['search'], get_search_query()) . $after;
			}
		} elseif ( is_day() ) {
			if ( $show_home_link ) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
			echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
			if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
		} elseif ( is_month() ) {
			if ( $show_home_link ) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
			if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_time('Y') . $after;
		} elseif ( is_single() && !is_attachment() ) {
			if ( $show_home_link ) echo $sep;
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_url . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category();
				$cat = $cat[0];
				$cats = get_category_parents($cat, true, $sep);
				if ( !$show_current || get_query_var('cpage') ) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
				echo $cats;
				if ( get_query_var('cpage') ) {
					echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
				} else {
					if ( $show_current ) echo $before . get_the_title() . $after;
				}
			}
			// custom post type
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			if ( get_query_var('paged') ) {
				echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ( $show_current ) echo $sep . $before . $post_type->label . $after;
			}
		} elseif ( is_attachment() ) {
			if ( $show_home_link ) echo $sep;
			$parent = get_post($parent_id);
			$cat = get_the_category($parent->ID);
			$cat = $cat[0];
			if ( $cat ) {
				$cats = get_category_parents($cat, true, $sep);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats);
				echo $cats;
			}
			printf($link, get_permalink($parent), $parent->post_title);
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && !$parent_id ) {
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && $parent_id ) {
			if ( $show_home_link ) echo $sep;
			if ( $parent_id != $frontpage_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page = get_page($parent_id);
					if ( $parent_id != $frontpage_id ) {
						$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ( $i != count($breadcrumbs) - 1 ) echo $sep;
				}
			}
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_tag() ) {
			if ( get_query_var('paged') ) {
				$tag_id = get_queried_object_id();
				$tag = get_tag($tag_id);
				echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ( $show_current ) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
			}
		} elseif ( is_author() ) {
			global $author;
			$author = get_userdata($author);
			if ( get_query_var('paged') ) {
				if ( $show_home_link ) echo $sep;
				echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf($text['author'], $author->display_name) . $after;
			}
		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . $text['404'] . $after;
		} elseif ( has_post_format() && !is_singular() ) {
			if ( $show_home_link ) echo $sep;
			echo get_post_format_string(get_post_format());
		}
		echo $wrap_after;
	}
}

function reviewView($ID)
{
	$post = get_post($ID);
	$review_image = get_field('review_image', $ID);
	$content = $post->post_content;
//	$content = apply_filters('the_content', $content);
	$review = '<div class="review"><div class="wrap row">
				<div class="col-md-4 photo-review">
					<div class="image"
					     style="background-image: url(' . $review_image['sizes']['medium'] . ')"></div>
				</div>
				<div class="col-md-2 line-wrap">
					<img src="/wp-content/themes/devdigital/assets/images/lines.svg">
				</div>
				<div class="col-md-6 review-info">
					<div class="name">' . get_field('review_name', $ID) . '</div>
					<div class="post">' . get_field('review_post', $ID) . '</div>
					<div class="text">' . $content . '<p><a target="_blank" href="' . get_the_permalink($ID) . '">Читать далее</p></a>
					</div>
				</div>
			</div></div>';
	return $review;
}

function frontImage($type, $size)
{
	global $wpdb;
	$query = "SELECT post_id FROM $wpdb->postmeta WHERE `meta_key` = 'project_front_position' AND `meta_value` = '$type'";
	$result = $wpdb->get_results($query);
	$image = get_field('project_front_image', $result[0]->post_id);
	$image_src = wp_get_attachment_image_src($image['ID'], $size);
	$return_array['ID'] = $result[0]->post_id;
	$return_array['image'] = $image_src[0];
	return $return_array;
}

if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => 'Языковая настройка',
		'menu_title' => 'Языковая настройка',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	));
	acf_add_options_page(array(
		'page_title' => 'Подвал сайта',
		'menu_title' => 'Подвал сайта',
		'menu_slug'  => 'theme-general-settings-footer',
		'capability' => 'edit_posts',
		'redirect'   => false
	));
}
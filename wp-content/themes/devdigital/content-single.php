<?php
/**
 * @package _mbbasetheme
 */
?>
<div class="marginbottom">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content row">
			<?= the_content() ?>
		</div>
	</article>
</div>
<?php
/**
 * The template for displaying the front page.
 *
 * This is the template that displays on the front page only.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<section class="hello-block">
	<svg width="100%" height="100%" id="mainHello" viewBox="0 0 600 405" preserveAspectRatio="xMinYMin meet">
		<defs>
			<filter id="filter-hello">
				<feGaussianBlur in="SourceAlpha" stdDeviation="2.2" />
				<feOffset dx="0" dy="0" result="offsetblur" />
				<feFlood flood-color="rgba(0,0,0,0.4)" />
				<feComposite in2="offsetblur" operator="in" />
				<feMerge>
					<feMergeNode />
					<feMergeNode in="SourceGraphic" />
				</feMerge>
			</filter>
		</defs>
		<path d="M0,0 L 0,298 225,400 590,0 0,0 Z" id="svgMain" vector-effect="non-scaling-stroke"
		      filter="url(#filter-hello)" />
		<g transform="translate(235,0)" class="textWrap">
			<g transform="translate(-45,0)">
				<foreignObject width="40%" height="33%" y="25%">
					<body xmlns="http://www.w3.org/1999/xhtml">
					<header class="page-title">
						<hgroup>
							<div class="title-wrap">
								<h1><?= get_field('mainblock_title') ?></h1>
								<div class="wrap-inner">
									<h2><?= get_field('mainblock_description') ?></h2>
								</div>
							</div>
						</hgroup>
					</header>
					</body>
				</foreignObject>
			</g>
			<g class="text_field" transform="translate(-45,127)">
			</g>
			<g>
				<line transform="rotate(45 60 116)" x1="0" y1="0" x2="0" y2="350" stroke-width="20"
				      stroke="rgb(255,255,255)"></line>
				<line transform="rotate(45 60 130)" x1="0" y1="0" x2="0" y2="350" stroke-width=".3"
				      stroke="rgb(0,0,0)"></line>
				<line transform="rotate(45 60 134)" x1="0" y1="0" x2="0" y2="80" stroke-width=".3" stroke="rgb(0,
					0,0)"></line>
			</g>
			<g transform="translate(-65,165)" width="100" class="hover_group">
				<rect x="0" y="0" width="100" height="15" transform="skewX(-45)"></rect>
				<text x="15" y="8" font-size="8" text-anchor="start"
				      alignment-baseline="middle"><?= get_field('connect_with_us', 'option') ?></text>
			</g>
		</g>
	</svg>
	<div id="mobileHello">
		<div class="container">
			<header class="page-title">
				<hgroup>
					<div class="title-wrap">
						<h1><?= get_field('mainblock_title') ?></h1>
						<div class="wrap-inner">
							<h2><?= get_field('mainblock_description') ?></h2>
							<div class="button">
								<a href="#"><?= get_field('connect_with_us', 'option') ?></a>
							</div>
						</div>
					</div>
				</hgroup>
			</header>
		</div>
	</div>
</section>
<section class="card-block" id="card-block"></section>
<section class="aboutus-block">
	<svg width="100%" id="mainAbout" height="100%" viewBox="0 0 600 405" preserveAspectRatio="xMinYMin meet">
		<defs>
			<filter id="filter-aboutus">
				<feGaussianBlur in="SourceAlpha" stdDeviation="2.2"></feGaussianBlur>
				<feOffset dx="1" dy="1" result="offsetblur"></feOffset>
				<feFlood flood-color="rgba(0,0,0,0.5)"></feFlood>
				<feComposite in2="offsetblur" operator="in"></feComposite>
				<feMerge>
					<feMergeNode></feMergeNode>
					<feMergeNode in="SourceGraphic"></feMergeNode>
				</feMerge>
			</filter>
		</defs>
		<path id="static-patch" d="M 0,180 L 0,405 600,375 600,0 350,230 0,180 Z"
		      vector-effect="non-scaling-stroke" filter="url(#filter-aboutus)"></path>
		<path d="M 0,80 L 0,325 600,395 600,100 80,50 0,80 Z" vector-effect="non-scaling-stroke"
		      filter="url(#filter-aboutus)"></path>
		<g class="textWrap">
			<foreignObject width="70%" height="45%" y="35%" x="82">
				<div class="content" xmlns="http://www.w3.org/1999/xhtml">
					<h2><?= get_field('aboutus_title') ?></h2>
					<div class="description-page">
						<span class="shape"></span>
						<?= get_field('aboutus_description') ?>
					</div>
				</div>
			</foreignObject>
			<g class="text_field" transform="translate(238,160)"></g>
			<g transform="translate(200,90)">
				<line transform="rotate(45 60 123)" x1="0" y1="0" x2="0" y2="300" stroke-width="10"
				      stroke="rgb(255,255,255)"></line>
				<line transform="rotate(45 60 130)" x1="0" y1="0" x2="0" y2="330" stroke-width=".3"
				      stroke="rgb(0,0,0)"></line>
				<line transform="rotate(45 60 134)" x1="0" y1="0" x2="0" y2="50" stroke-width=".3"
				      stroke="rgb(0,0,0)"></line>
			</g>
			<g transform="translate(110,280)" width="100" class="hover_group">
				<rect x="0" y="0" width="100" height="15" transform="skewX(-45)"></rect>
				<text x="15" y="8" font-size="8" text-anchor="start"
				      alignment-baseline="middle"><?= get_field('readmore', 'option') ?></text>
			</g>
		</g>
	</svg>
	<div id="mobileAbout">
		<div class="container">
			<div class="content">
				<h2><?= get_field('aboutus_title') ?></h2>
				<div class="description-page">
					<?= get_field('aboutus_description') ?>
				</div>
				<div class="readmore">
					<a href="/we/" class="blue-button"><?= get_field('readmore', 'option') ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	// WHERE DON'T WORK FOREIGNOBJECT IN BROWSER
	var foreignDontWork = {
		"helloblock": [
			{
				"title": <?= json_encode(get_field('mainblock_title')) ?>,
				"description": <?= json_encode(strip_tags(str_replace("<br />", "***", str_replace("\n", "", get_field('mainblock_description'))))) ?>
			}
		],
		"aboutus": [
			{
				"title": <?= json_encode(get_field('aboutus_title')) ?>,
				"description": <?= json_encode(strip_tags(str_replace("\n", "", get_field('aboutus_description')))) ?>
			}
		]

	};
</script>
<?php
$arg = array(
	'post_type'   => 'services',
	'post_parent' => 0
);
$query = new WP_Query($arg);
// Цикл
if ( $query->have_posts() ) {
	?>
	<section class="category-icon">
		<div class="container">
			<ul class="category-list row">
				<?php
				while ( $query->have_posts() ) {
					$query->the_post();
					echo '<li class="col-md-2 col-xs-12"><a href="' . get_the_permalink() . '">' . get_field('sercives') . '</a></li>';
				}
				?>
			</ul>
		</div>
	</section>
	<?php
}
wp_reset_postdata();
?>
<section class="projects-block">
	<div class="row wrap-project-list">
		<div class="col-md-5 col-xs-12 block block-1">
			<?php
			$image_link = frontImage('first', array(800, 700));
			?>
			<a href="<?= get_permalink($image_link['ID']) ?>">
				<div class="image"
				     style="background-image: url('<?= $image_link['image'] ?>')">
					<div class="overlay">
						<div class="arrow-left"></div>
						<div class="rectangle"></div>
					</div>
					<div class="description">
						<h2><?= get_the_title($image_link['ID']) ?></h2>
						<h3><?= get_field('project_description', $image_link['ID']) ?></h3>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 col-xs-12 block block-2">
			<div class="row">
				<?php
				$image_link = frontImage('two', array(350, 350));
				?>
				<a href="<?= get_permalink($image_link['ID']) ?>" class="col-md-6 col-xs-12 col-sm-6 image-wrap-link">
					<div class="image"
					     style="background-image: url('<?= $image_link['image'] ?>')">
						<div class="overlay">
							<div class="arrow-left"></div>
							<div class="rectangle"></div>
						</div>
						<div class="description">
							<h2><?= get_the_title($image_link['ID']) ?></h2>
							<h3><?= get_field('project_description', $image_link['ID']) ?></h3>
						</div>
					</div>
				</a>
				<?php
				$image_link = frontImage('three', array(350, 350));
				?>
				<a href="<?= get_permalink($image_link['ID']) ?>" class="col-md-6 col-xs-12  col-sm-6 image-wrap-link">
					<div class="image"
					     style="background-image: url('<?= $image_link['image'] ?>')">
						<div class="overlay">
							<div class="arrow-left"></div>
							<div class="rectangle"></div>
						</div>
						<div class="description">
							<h2><?= get_the_title($image_link['ID']) ?></h2>
							<h3><?= get_field('project_description', $image_link['ID']) ?></h3>
						</div>
					</div>
				</a>
			</div>
			<div class="row">
				<?php
				$image_link = frontImage('four', array(700, 350));
				?>
				<a href="<?= get_permalink($image_link['ID']) ?>">
					<div class="image" style="background-image: url('<?= $image_link['image'] ?>')">
						<div class="overlay">
							<div class="arrow-left"></div>
							<div class="rectangle"></div>
						</div>
						<div class="description">
							<h2><?= get_the_title($image_link['ID']) ?></h2>
							<h3><?= get_field('project_description', $image_link['ID']) ?></h3>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-3 col-xs-12 block block-4">
			<?php
			$image_link = frontImage('five', array(500, 700));
			?>
			<a href="<?= get_permalink($image_link['ID']) ?>">
				<div class="image"
				     style="background-image: url('<?= $image_link['image'] ?>')">
					<div class="overlay">
						<div class="arrow-left"></div>
						<div class="rectangle"></div>
					</div>
					<div class="description">
						<h2><?= get_the_title($image_link['ID']) ?></h2>
						<h3><?= get_field('project_description', $image_link['ID']) ?></h3>
					</div>
				</div>
			</a>
		</div>
	</div>
</section>
<?php get_footer(); ?>

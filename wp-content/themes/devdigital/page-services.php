<?php get_header(); ?>

<?php
include "lib/inc/page-header.php";

$arg = array(
	'post_type'   => 'services',
	'post_parent' => 0
);
$query = new WP_Query($arg);
// Цикл
if ( $query->have_posts() ) {
	?>
	</div>
	</main>
	<div class="services">
		<div class="container">
			<ul class="row">
				<?php
				while ( $query->have_posts() ) {
					$query->the_post();
					$current_id = get_the_ID();
					echo '<li class="col-md-2 item-service"><a href="' . get_the_permalink() . '">' . get_field('sercives') . '</a>';
					$posts = get_posts(array(
						'numberposts' => -1, // тоже самое что posts_per_page
						'post_type'   => 'services',
						'post_parent' => $current_id,
						'post_status' => 'publish'
					));

					if ( $posts ) {
						echo '<ul class="subservice-list">';
						foreach ($posts as $post) {
							setup_postdata($post);
							echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
						}
						echo '</ul>';
					}
					wp_reset_postdata();

					echo '</li>';
				}
				?>
			</ul>
		</div>
	</div>
	<?php
}
wp_reset_postdata();
?>
<?php get_footer(); ?>
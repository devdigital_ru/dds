<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package _mbbasetheme
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="text404">404</h1>
					<h1 class="page-title"><?= get_field('title404', 'option') ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?= get_field('description404', 'option') ?></p>

					<?php get_search_form(); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<div class="callbackForm">

	<div class="form">
		<svg width="100%" height="100%" id="ContactFormSVG" viewBox="0 0 300 700" preserveAspectRatio="none">
			<defs>
				<filter id="filter-contact-form">
					<feGaussianBlur in="SourceAlpha" stdDeviation="3"></feGaussianBlur>
					<feOffset dx="0" dy="0" result="offsetblur"></feOffset>
					<feFlood flood-color="rgba(0,0,0,0.4)"></feFlood>
					<feComposite in2="offsetblur" operator="in"></feComposite>
					<feMerge>
						<feMergeNode></feMergeNode>
						<feMergeNode in="SourceGraphic"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<polygon points="306,710 306,-10 5,335" stroke="purple" fill="white" stroke-width="0" vector-effect="non-scaling-stroke" filter="url(#filter-contact-form)"></polygon>
			<g transform="translate(25,320)" class="openContactForm">
				<image width="27" height="25" class="contact-form-image mail"
				       xmlns:xlink="http://www.w3.org/1999/xlink"
				       xlink:href="/wp-content/themes/devdigital/assets/images/maill.svg">
				</image>
			</g>

		</svg>

		<div class="form-wrap">
			<div class="hidden mobile-label">
				<div id="ContactFormSVGMobile"></div>
			</div>
			<?php
			// ВЫВОД ФОРМЫ ОБРАТНОЙ СВЯЗИ
			languageFeedbackForm();
			?>
		</div>

	</div>
</div>
<?php include_once("analyticstracking.php") ?>
<?php wp_footer(); ?>
</body>
</html>


<?php
/**
 * @package _mbbasetheme
 */

?>
<?php
if (get_the_content()){
?>
<div class="we">
	<section id="aboutus" class="aboutus">
		<header>
			<hgroup>
				<h2><?php the_field('title_aboutus'); ?></h2>
				<h3><?php the_field('subtitle_aboutus'); ?></h3>
			</hgroup>
		</header>
		<aside>
			<?php the_content(); ?>
		</aside>
	</section>
	<?php } ?>
	<?php
	// ФИЛВОРД
	$feelword = get_field('filword');
	if ( $feelword ) {
		?>
		<section id="wordcard" class="wordcard">
			<header>
				<hgroup>
					<h2><?php the_field('title_filword'); ?></h2>
					<h3><?php the_field('subtitle_filword'); ?></h3>
				</hgroup>
			</header>
			<aside>
				<div class="words">
					<div class="words-list">
						<?php
						foreach ($feelword as $key => $fw) {
							?>
							<a href="#" alt="<?= $fw["title_word"] ?>" class="naoTooltip-wrap"><span
									data-active-words="<?= $key ?>" title="<?= $fw["title_word"] ?>"><?= $fw["word"] ?></span>
							</a>
							<?php
						}
						?>
					</div>
					<div class="words-wrap row">
						<?php
						$symbol = languageWeSymbol();
						$c = count($symbol); // длина алфавита для рандома
						$widthFilword = 25;
						$heightFilword = 16;
						$tmpArray = array();
						// создаем временный массив, для размещения слов
						for ($i = 0; $i < $heightFilword; $i++) {
							for ($j = 0; $j < $widthFilword; $j++) {
								$tmpArray[$i][$j] = '-';
							}
						}
						// проходим по словам
						foreach ($feelword as $key => $fw) {
							$result = 0;
							while ( !$result ) {
								$result = 1;
								$position = positionWordArray($fw["word"], $widthFilword, $heightFilword);
								/*
								 * проверяем свободны ли клетки
								 *
								 */
								for ($i = $position['line']; $i < $position['newLine']; $i++) {
									for ($j = $position['column']; $j < $position['newColumn']; $j++) {
										if ( $tmpArray[$i][$j] != '-' && ($tmpArray[$i + 1][$j] != '-' || $tmpArray[$i - 1][$j] != '-' || $tmpArray[$i][$j + 1] != '-' || $tmpArray[$i][$j - 1] != '-') )
											$result = 0;
//                                        if (($position['align'] == 1 && $tmpArray[$i][$j] !== '-') ||
//                                            ($position['align'] == 2 && $tmpArray[$j][$i] !== '-'))
//                                            $result = 0;
									}
								}
//                                $array_word = str_split($fw["word"]);
								$array_word = preg_split('//u', $fw["word"], -1, PREG_SPLIT_NO_EMPTY);
								if ( $result ) {
									$kk = 0;
									for ($i = $position['line']; $i < $position['newLine']; $i++) {
										for ($j = $position['column']; $j < $position['newColumn']; $j++) {
//                                            if ($position['align'] == 1)
											$tmpArray[$i][$j] = '<span data-word="' . $key . '">' . $array_word[$kk] . '</span>';
//                                            else
//                                                $tmpArray[$j][$i] = '<span data-word="' . $key . '">' . $array_word[$kk] . '</span>';
											$kk++;
										}
									}
								}
							}
						}
						for ($i = 0; $i < $heightFilword; $i++) {
							for ($j = 0; $j < $widthFilword; $j++) {
								if ( $tmpArray[$i][$j] == '-' )
									$tmpArray[$i][$j] = $symbol[rand(0, $c - 1)];
							}
						}
						for ($i = 0; $i < $heightFilword; $i++) {
							echo '<div class="row">';
							for ($j = 0; $j < $widthFilword; $j++) {
//								echo '<div class="element">' . $symbol[rand(0, $c - 1)] . '</div>';
								echo '<div class="element gray-color transform">' . $tmpArray[$i][$j] . '</div>';
							}
							echo '</div>';
						}
						?>
					</div>
				</div>
			</aside>
		</section>
	<?php } ?>
	<?php
	//ЭТО МЫ
	$itsme = get_field('staff');
	if ( $itsme ) {
		?>
		<section class="itsme">
			<header>
				<hgroup>
					<h2><?php the_field('title_staff'); ?></h2>
					<h3><?php the_field('subtitle_staff'); ?></h3>
				</hgroup>
			</header>
			<aside>
				<div class="itsme-list row">
					<?php
					// заменить i в тексте на key
					foreach ($itsme as $key => $list) { ?>
						<div class="item-itsme col-md-4">
							<a id="itsme<?= $key ?>" class="itsme-popup" data-item="<?= $key ?>" href="#animatedModal">
								<div class="small-image"
								     style="background-image: url('<?= $list["photo"]['url'] ?>')"></div>
							</a>
						</div>
						<?php
					} ?>
				</div>
				<div id="animatedModal">
					<div id="closebt-container" class="close-animatedModal">
						<img class="closebt" src="<?= bloginfo('template_url') ?>/assets/images/closebt.svg">
					</div>
					<div class="modal-content container-fluid">
						<?php
						// заменить i в тексте на key
						foreach ($itsme as $key => $list) {
							?>
							<div class="itsme-more row hidden" id="itsme-more-<?= $key ?>">
								<?php
								$image = '<img src="' . $list["photo_big"]["sizes"]["large"] . '">';
								$FioPostDescription = '<p class="full-name">' . $list["first_last_name"] . '</p><p
                                    class="post">' . $list["post"] . '</p><p class="description">' . $list["description"] . '</p>';
								$Education = '<p class="education">' . $list["education"] . '</p>';
								$Social = '';
								if ( $list["email"] || $list["social_links"] ) {
									$Social = '<ul class="email-social-list">';
									if ( $list["email"] ) {
										$themeMail ='For '.$list["first_last_name"];
										$Social .= '<li>
                                            <a href="mailto:' . $list["email"] . '?subject='.$themeMail.'"><span
                                            class="social-icon" data-color="#f6a42b"><i class="fa fa-envelope" aria-hidden="true"></i></span></a></li>';
									}
									if($list["social_links"]) {
										foreach ($list["social_links"] as $descr) {
											$Social .= '<li>
                                                <a href="' . $descr['link_social'] . '" target="_blank" rel="nofollow"><span
                                                class="social-icon" data-color="' . $descr['color_social'] . '"><i class="fa ' . $descr["icons_social"] . '" aria-hidden="true"></i></span></a></li>';
										}
									}
									$Social .= '</ul>';
								}
								$EducationSocial = $Education . $Social;
								switch ( $list['position_photo'] ) {
									case 'left':
										?>
										<div class="item__image col-md-4"><?= $image ?> </div>
										<div
											class="item__description right col-md-8"><?= $FioPostDescription .
											$EducationSocial ?></div>
										<?php
										break;
									case 'center':
										?>
										<div class="item__description left col-md-4"><?= $FioPostDescription ?></div>
										<div class="item__image col-md-4"><?= $image ?> </div>
										<div class="item__description right col-md-4"><?= $EducationSocial ?></div>
										<?php
										break;
									case 'right':
										?>
										<div
											class="item__description left col-md-8"><?= $FioPostDescription .
											$EducationSocial ?></div>
										<div class="item__image col-md-4"><?= $image ?> </div>
										<?php
										break;
								}
								?>
							</div>
							<?php
						} ?>
					</div>
				</div>
			</aside>
		</section>
	<?php } ?>

	<?php
	$chronicle = get_field('chronicle');
	if ( $chronicle ) {
		?>
		<section class="chronicle">
			<header>
				<hgroup>
					<h2><?php the_field('title_chronicle'); ?></h2>
					<h3><?= replaceYMD(get_field('subtitle_chronicle'), $current_language); ?></h3>
				</hgroup>
			</header>
			<aside>
				<div class="chronicle-list row">
					<?php
					foreach ($chronicle as $ch) {
						?>
						<div class="col-md-4 item">
							<p class="value<?= $ch['class'] ? ' ' . $ch['class'] : '' ?>"><?= $ch['value'] ?></p>
							<p class="description"><?= $ch['description'] ?></p>
						</div>
						<?php
					}
					?>
				</div>
			</aside>
		</section>
	<?php } ?>
	<?php
	$certificates = get_field('sertificats_list');
	if ( $certificates ) {
		?>
		<section class="certificates">
			<header>
				<hgroup>
					<h2><?php the_field('title_certificates'); ?></h2>
					<h3><?php the_field('subtitle_certificates'); ?></h3>
				</hgroup>
			</header>
			<aside>
				<div class="certificates-list row">
					<?php
					foreach ($certificates as $sert) {
						?>
						<div class="item col-md-3">
							<a class="popup-gallery" href="<?= $sert['sertificat']["sizes"]["large"] ?>"
							   title="<?= $sert["sertificat_name"] ?>" alt="<?= $sert["sertificat_name"] ?>">
								<div class="item__image"
								     style="background-image: url('<?= $sert['sertificat']["sizes"]["medium"] ?>')"></div>
								<h4><?= $sert["sertificat_name"] ?></h4>
							</a>
						</div>
					<?php } ?>
				</div>
			</aside>
		</section>
	<?php } ?>
</div>
"use strict";
var gulp = require('gulp'),
	mainBowerFiles = require('main-bower-files'),
	autoprefixer = require('gulp-autoprefixer');


gulp.task('default', function () {
	gulp.start('mainCSS','mainLESS', 'autoprefixer', 'mainJS', 'mainFONT', 'mainIMG');
});

//
//gulp.task('clean', function(cb) {
//	del('assets', cb);
//});

gulp.task('mainCSS', function() {
	return gulp.src(mainBowerFiles('**/*.css'))
		.pipe(gulp.dest('assets/css'));
});

gulp.task('mainLESS', function() {
	return gulp.src(mainBowerFiles('**/*.less'))
		.pipe(gulp.dest('assets/css/less'));
});

gulp.task('autoprefixer', function () {
	return gulp.src('assets/css/*.css')
		.pipe(autoprefixer({
			browsers: ['last 3 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('assets/css'));
});


gulp.task('mainFONT', function() {
	return gulp.src(mainBowerFiles(['**/*.woff', '**/*.ttf', '**/*.eot']))
		.pipe(gulp.dest('assets/css/fonts'));
});


gulp.task('mainIMG', function() {
	return gulp.src(mainBowerFiles(['**/*.gif', '**/*.jpg', '**/*.png']))
		.pipe(gulp.dest('assets/img'));
});



gulp.task('mainJS', function() {
	return gulp.src(mainBowerFiles('**/*.js'))
		.pipe(gulp.dest('assets/js'));
});

<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */
get_header(); ?>

<?php
global $post;
?>
<div class="marginbottom blog row">
	<div class="col-md-8 blog-list-article">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part('content', 'blogpage'); ?>
			<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				$current_language = qtranxf_getLanguage();
				echo '<div class="comments-wrap">';
				switch ($current_language) {
					case 'ru':
						echo '<h2>Комментарии</h2>';
						break;
					default:
						echo '<h2>Comments</h2>';
				}

				comments_template();
				echo '</div>';
			endif;
			?>
		<?php endwhile; // end of the loop. ?>

	</div>
	<div class="col-md-4 sidebar-blog">
		<?php  dynamic_sidebar( 'secondary-widget-area' ); ?>
	</div>
	<div class="last-read"><span>Вы вернулись, чтобы дочитать статью? Кликните на меня, я найду то место, где вы остановились.</span></div>
</div>
<?php get_footer(); ?>

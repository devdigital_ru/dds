<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part('content', 'service-inner'); ?>
			<?php
			if ( wp_get_post_parent_id(get_the_ID()) != 0 ) {
			?>
			<section class="service-navigation">
				<?php _mbbasetheme_post_nav(); ?>
			</section>
		<?php } ?>
		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

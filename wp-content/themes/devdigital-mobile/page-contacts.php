<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */

get_header(); ?>
<header id="header">
	<hgroup>
		<div class="container">
			<h1><?= the_title() ?></h1>
			<div class="wrap-inner">
				<h2><?= get_field('subtitle') ?></h2>
				<div class="callback-we">
					<a href="#" class="blue-button"><?= get_field('connect_with_us','option')?></a>
				</div>
			</div>
		</div>
	</hgroup>
</header>
<section id="content">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'contacts' ); ?>

		<?php endwhile; // end of the loop. ?>

		<?php get_footer(); ?>

<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php
		//	include "lib/inc/page-header.php";
			?>

			<?php get_template_part( 'content', 'sale-inner' ); ?>

			<?php _mbbasetheme_post_nav(); ?>

		<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */

get_header(); ?>
<header id="header">
	<hgroup>
		<div class="container">
			<h1><?= the_title() ?></h1>
			<div class="wrap-inner">
				<h2><?= get_field('subtitle') ?></h2>
				<div class="callback-we">
					<a href="#" class="blue-button"><?= get_field('connect_with_us','option')?></a>
				</div>
			</div>
		</div>
	</hgroup>
</header>
<section id="content">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			$args = array(
				'post_type'      => 'abrakadabra',
				'posts_per_page' => 15
			);
			$query = new WP_Query($args);
// Цикл
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					echo '</div><div class="container-fluid"><div class="row abrakadabra-list">';
					get_template_part('content', 'abrakadabra');
					echo '</div></div><div class="container">';
				}
			}
			wp_reset_postdata();
			?>
		<?php endwhile; // end of the loop. ?>

		<?php get_footer(); ?>

$(document).ready(function () {
	$('.nav').slideAndSwipe({
		triggerOnTouchEnd: true,
		allowPage: 'vertical',
		threshold: 100,
		excludedElements: 'label, button, input, select, textarea, .noSwipe',
		speed: 250

	});

	$('.spinner-spin').click(function () {
		if (!$("#spinner-form").prop("checked")) {
			$("#spinner-form").prop("checked", true);
		} else {
			$("#spinner-form").prop("checked", false);
		}
	});

	$('.widget.qtranxs_widget').click(function (e) {
		if (!$(this).hasClass('open')) {
			e.preventDefault();
			$(this).addClass('open');
		} else {
			$(this).removeClass('open');
		}
	});

	$(document).mouseup(function (e) {
		var div = $(".language-chooser.language-chooser-text.qtranxs_language_chooser");
		if (!div.is(e.target) && div.has(e.target).length === 0) {
			$('.widget.qtranxs_widget').removeClass('open');
		}
	});

});
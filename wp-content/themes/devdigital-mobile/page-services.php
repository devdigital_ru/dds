<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _mbbasetheme
 */

get_header(); ?>
<header id="header">
	<hgroup>
		<div class="container">
			<h1><?= the_title() ?></h1>
			<div class="wrap-inner">
				<h2><?= get_field('subtitle') ?></h2>
				<div class="callback-we">
					<a href="#" class="blue-button"><?= get_field('connect_with_us','option')?></a>
				</div>
			</div>
		</div>
	</hgroup>
</header>
<section id="content">
	<div class="container">

		<?php

		$arg = array(
			'post_type'   => 'services',
			'post_parent' => 0
		);
		$query = new WP_Query($arg);
		// Цикл
		if ( $query->have_posts() ) {
		?>
			<div class="services-mobile">
				<ul class="col-group">

					<?php
					while ( $query->have_posts() ) {
						$query->the_post();
						$current_id = get_the_ID();
						echo '<li class="item-service col-4"><a href="' . get_the_permalink() . '">' . get_field('sercives') . '</a>';
						$posts = get_posts(array(
							'numberposts' => -1, // тоже самое что posts_per_page
							'post_type'   => 'services',
							'post_parent' => $current_id,
							'post_status' => 'publish'
						));

						if ( $posts ) {
							echo '<ul class="subservice-list">';
							foreach ($posts as $post) {
								setup_postdata($post);
								echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
							}
							echo '</ul>';
						}
						wp_reset_postdata();

						echo '</li>';
					}
					?>
				</ul>
			</div>
			<?php
		}
		wp_reset_postdata();
		?>


		<?php get_footer(); ?>

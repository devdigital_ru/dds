<?php
/**
 * The template for displaying all single posts.
 *
 * @package _mbbasetheme
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php
//	include "lib/inc/page-header.php";
	?>
<?php endwhile; // end of the loop. ?>
</div>
</main>
<?php
function titleDescription($title, $description)
{
	return '<h2>' . $title . '</h2><div class="description">' . $description . '</div>';
}

function mediaContent($media)
{
	switch ( $media["acf_fc_layout"] ) {
		case "image":
			$image_url = wp_get_attachment_image_src($media["image"], 1000, 1000);
			return '<div class="image" style="background-image: url(\'' . $image_url[0] . '\')"></div>';
			break;
		case "prototype-maket":
			return '<div class="twentytwenty-container">
					<img src="' . $media["image-prototype"]["url"] . '">
					<img src="' . $media["image-maket"]["url"] . '">
					</div>';
			break;
	}
}

?>
<div class="container-inner-project container-fluid marginbottom">
	<?php
	$image_url = get_field('project_preview');
	if ( $image_url ) {
		?>
		<div class="image-project"
		     style="background-image: url('<?= $image_url['url'] ?>')"></div>
	<?php } ?>
	<div class="main-information-project">
		<div class="row">
			<div class="col-md-4 left-column">
				<time class="date-project"><?= get_the_date() ?></time>
				<p class="name-client"><?= get_the_title() ?></p>
				<div class="share">share</div>
			</div>
			<div class="col-md-8 right-column">
				<h2><?= get_field('project_name') ?></h2>
				<?php $site_link = get_field('project_site');
				if ( $site_link ) { ?>
				<a href="<?= $site_link ?>" target="_blank" rel="nofollow" class="link-site button-link">перейти на
						сайт</a><?php
				} ?>
				<?php
				$posttags = get_the_tags();
				if ( $posttags ) {
					echo '<ul class="tags">';
					foreach ($posttags as $tag) {
						?>
						<li><a href="#"><?= $tag->name ?></a></li>
						<?php
					}
					echo '</ul>';
				}
				?>
				<div class="description"><?= get_field('project_fulldescription'); ?></div>
			</div>
		</div>
	</div>
	<?php
	$layouts = get_field('layouts');
	if ( $layouts && !$is_IE ) {
		?>
		<div class="layouts-adaptive-project">
			<?php
			$layout_PC = $layout_macbook = $layout_tablet = $layout_phone = false;
			foreach ($layouts as $lay) {
				if ( $lay["PC"] )
					$layout_PC = true;
				if ( $lay["macbook"] )
					$layout_macbook = true;
				if ( $lay["laptop"] )
					$layout_tablet = true;
				if ( $lay["phone"] )
					$layout_phone = true;
			}
			if ( $layout_PC || $layout_macbook || $layout_tablet || $layout_phone ) {
				?>
				<ul class="view">
					<?php if ( $layout_PC ) { ?>
						<li class="active" data-swicth="PC">ПК</li>
					<?php } ?>
					<?php if ( $layout_macbook ) { ?>
						<li data-swicth="Notebook">Ноутбук</li>
					<?php } ?>
					<?php if ( $layout_tablet ) { ?>
						<li data-swicth="Plane">Планшет</li>
					<?php } ?>
					<?php if ( $layout_phone ) { ?>
						<li data-swicth="Phone">Телефон</li>
					<?php } ?>
				</ul>
			<?php } ?>
			<div class="wrap row">
				<div class="col-md-3 info-mockup">
					<ul class="page">
						<?php
						foreach ($layouts as $key => $lay) {
							?>
							<li<?= $key == 0 ? ' class="active"' : '' ?> data-index="<?= $key ?>"><span class="name"><?=
									$lay["layout_name"] ?></span>
								<span
									class="number"><?php if ( ($key + 1) < 10 ) echo '0' . ($key + 1); else ($key + 1); ?></span>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
				<div class="col-md-9 design-view">
					<div class="mockup-pc">
						<?php if ( $layout_PC ) { ?>
							<!--		iMAC			-->
							<div class="device active" data-swicth="PC">
								<svg viewBox="0 122 1000 770" preserveAspectRatio="xMinYMin meet">
									<g>
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="498" cy="142.5"
										        r="3.8" />
										<g>
											<foreignObject x="0" y="0" width="1155" height="650" preserveAspectRatio
											               transform="matrix(0.7614 0 0 0.7614 58.0533 161.4387)">
												<div class="layouts-list">
													<?php
													foreach ($layouts as $key => $lay) {
														?>
														<img src="<?= $lay["PC"]['url'] ?>"
														     data-index="<?= $key ?>" style="width: 100%;height: auto;">
													<?php } ?>
												</div>
											</foreignObject>
										</g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M947.9,781.3H48.1c-15.7,0-28.5-12.8-28.5-28.5V150.9
		c0-15.7,12.8-28.5,28.5-28.5h899.9c15.7,0,28.5,12.8,28.5,28.5v601.9C976.4,768.5,963.7,781.3,947.9,781.3z" />
										<rect x="58.4" y="161.8" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="878.6"
										      height="494.2" />
										<line fill="none" stroke="#929292" stroke-miterlimit="10" x1="19.9" y1="692.5"
										      x2="976.2"
										      y2="692.5" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M614.9,878l36.2,8.3l1.4,0.3c0,0-0.5,0.8-1.3,0.8
		c-13.2,0.2-120.2,0.7-152.8,0.7c-38.8,0-136.1-0.5-151.2-0.7c-1.3,0-2-0.8-2-0.8l37-8.5c1.7-0.4,3.1-1.6,3.7-3.3
		c0.8-2.2,1.9-5.8,3.2-11.4c4.8-20.7,8.6-82.5,8.6-82.5h202.3c0,0,3.6,59.6,8.6,83.6c0.9,4.2,2.5,8.6,3.5,11
		C612.6,877,613.6,877.8,614.9,878z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M652.3,887.2c0,0-0.2,2.5-2.3,2.7c-13.1,0.9-120.2,0.7-152.8,0.7
		c-38.8,0-133-0.2-148.2-0.7c-4.2-0.2-4.3-2.9-4.3-2.9" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_macbook ) { ?>
							<!--		MACBOOK			-->
							<div class="device" data-swicth="Notebook">
								<svg x="0px" y="0px" viewBox="224 342 550 315" preserveAspectRatio="xMinYMin meet">
									<g>
										<g>
											<foreignObject x="0" y="0" width="821" height="513" preserveAspectRatio
											               transform="matrix(0.4994 0 0 0.4994 293.75 363)">
												<div class="layouts-list">
													<?php
													foreach ($layouts as $key => $lay) {
														?>
														<img src="<?= $lay["macbook"]['url'] ?>"
														     data-index="<?= $key ?>" style="width: 100%;height: auto;">
													<?php } ?>
												</div>
											</foreignObject>
										</g>
										<g id="camera_1_">
											<g>
												<ellipse fill="#FFFFFF" cx="499" cy="353.5" rx="2" ry="2" />
											</g>
											<g>
												<ellipse fill="none" stroke="#929292" stroke-miterlimit="10" cx="499"
												         cy="353.5"
												         rx="2" ry="2" />
											</g>
										</g>
										<rect x="293.8" y="363" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="410"
										      height="256.3" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M717.2,639.1H281.3c-1.2,0-2.1-3.4-2.1-4.6v-275
		c0-8.6,7-15.7,15.7-15.7h408.9c8.6,0,15.7,7,15.7,15.7v275C719.3,635.6,718.3,639.1,717.2,639.1z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M767.4,648.4H230.7c-0.8,0-1.4-0.6-1.4-1.4v-6.5
		c0-0.8,0.6-1.4,1.4-1.4h536.7c0.8,0,1.4,0.6,1.4,1.4v6.5C768.8,647.8,768.2,648.4,767.4,648.4z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M748.3,655.2H249.5c-3.8,0-19.4-3.1-19.4-6.9l0,0H768l0,0
		C768,652.1,752,655.2,748.3,655.2z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M530.6,643.8h-63.3c-2.5,0-4.5-2-4.5-4.5V639h72.3v0.3
		C535.1,641.8,533,643.8,530.6,643.8z" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_tablet ) { ?>
							<!--		iPAD			-->
							<div class="device" data-swicth="Plane">
								<svg x="0px" y="0px"
								     viewBox="150 180 650 640" preserveAspectRatio="xMinYMin meet">
									<g>
										<g>
											<foreignObject x="0" y="0" width="381" height="509" preserveAspectRatio
											               transform="matrix(1 0 0 1 308.0154 246.4975)">
												<div class="layouts-list plane">
													<?php
													foreach ($layouts as $key => $lay) {
														?>
														<img src="<?= $lay["laptop"]['url'] ?>"
														     data-index="<?= $key ?>" style="width: 100%;height: auto;">
													<?php } ?>
												</div>
											</foreignObject>
										</g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M693.3,811H305.1c-14,0-25.4-11.4-25.4-25.4V214.2
		c0-14,11.4-25.4,25.4-25.4h388.2c14,0,25.4,11.4,25.4,25.4v571.4C718.7,799.6,707.3,811,693.3,811z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M692.9,807.9H305.5c-12.5,0-22.5-10.1-22.5-22.5V214.5
		c0-12.5,10.1-22.5,22.5-22.5h387.3c12.5,0,22.5,10.1,22.5,22.5v570.9C715.4,797.8,705.3,807.9,692.9,807.9z" />
										<rect x="309.3" y="247.8" fill="none" stroke="#929292" stroke-miterlimit="10"
										      width="379.3"
										      height="507.3" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="499" cy="218.5"
										        r="3" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="498.9"
										        cy="782.7"
										        r="14.2" />
									</g>
								</svg>
							</div>
						<?php } ?>
						<?php if ( $layout_phone ) { ?>
							<!--		iPhone			-->
							<div class="device" data-swicth="Phone">
								<svg x="0px" y="0px" viewBox="200 250 600 500" preserveAspectRatio="xMinYMin meet">
									<g>
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M570.7,701.5H429.3c-16.2,0-29.3-13.1-29.3-29.3V327.8
		c0-16.2,13.1-29.3,29.3-29.3h141.3c16.2,0,29.3,13.1,29.3,29.3v344.4C600,688.4,586.9,701.5,570.7,701.5z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M571,698.3H427.5c-13.9,0-25.2-11.3-25.2-25.2V326.7
		c0-13.9,11.3-25.2,25.2-25.2H571c13.9,0,25.2,11.3,25.2,25.2V673C596.3,686.9,584.9,698.3,571,698.3z" />
										<g>
											<foreignObject x="410" y="348" width="177.5" height="305.5"
											               preserveAspectRatio>
												<div class="layouts-list phone">
													<?php
													foreach ($layouts as $key => $lay) {
														?>
														<img src="<?= $lay["phone"]['url'] ?>"
														     data-index="<?= $key ?>" style="width: 100%;height: auto;">
													<?php } ?>
												</div>
											</foreignObject>
										</g>
										<circle fill="#929292" cx="498.9" cy="312.1" r="2" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="467.9" cy="324"
										        r="3" />
										<path fill="#929292" d="M513.4,325h-26.9c-0.8,0-1.5-0.7-1.5-1.5l0,0c0-0.8,0.7-1.5,1.5-1.5h26.9c0.8,0,1.5,0.7,1.5,1.5l0,0
		C514.9,324.3,514.2,325,513.4,325z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M600.9,413.6H600v-29.1h0.9c0.4,0,0.7,0.3,0.7,0.7v27.6
		C601.7,413.3,601.3,413.6,600.9,413.6z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,413.3h1.8v-29.1h-1.8c-0.4,0-0.7,0.3-0.7,0.7v27.6
		C397.4,413,397.8,413.3,398.2,413.3z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,450.4h1.8v-29.1h-1.8c-0.4,0-0.7,0.3-0.7,0.7v27.6
		C397.4,450,397.8,450.4,398.2,450.4z" />
										<path fill="none" stroke="#929292" stroke-miterlimit="10" d="M398.2,365.7h1.8V349h-1.8c-0.4,0-0.7,0.3-0.7,0.7V365
		C397.4,365.4,397.8,365.7,398.2,365.7z" />
										<circle fill="none" stroke="#929292" stroke-miterlimit="10" cx="499.8"
										        cy="676.7"
										        r="14.9" />
									</g>
								</svg>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php
	$stage_work = get_field('stage_work');
	if ( $stage_work ) {
		?>
		<div class="tecnology-project">
			<div class="container">
				<header>
					<hgroup>
						<h2>Этапы работ</h2>
						<h3>Подзаголовок</h3>
					</hgroup>
				</header>
				<ul class="steps">
					<?php
					$count = count($stage_work);
					foreach ($stage_work as $key => $sw) {
						?>
						<li data-id="<?= ($key + 1) ?>"
							<?php
							if ( $key == 0 ) {
								echo ' class="first active"';
							} elseif ( $key == ($count - 1) ) {
								echo ' class="last"';
							}
							?>
						><span><?= ($key + 1) ?></span></li>
					<?php } ?>
				</ul>
				<ul class="info-steps">
					<?php
					foreach ($stage_work as $key => $sw) {
						?>
						<li<?= $key == 0 ? ' class="active"' : '' ?> data-item="<?= ($key + 1) ?>">
							<div class="wrap">
								<?= $sw['title'] ? '<h2>' . $sw['title'] . '</h2>' : '' ?>
								<?= $sw['description'] ? '<div class="description">' . $sw['description'] . '</div>' : '' ?>
								<?= $sw['image'] ? '<div class="image">' . wp_get_attachment_image($sw['image']['ID'],
										array(1100, 2000)) . '</div>' : '' ?>
							</div>
						</li>
						<?php
					}
					?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<?php
	$review_project = get_field('project_review');
	if($review_project){
	?>
	<div class="client-project">
		<div class="container">
			<?= reviewView($review_project->ID) ?>
		</div>
	</div>
	<?php } ?>
	<div class="tell-aboutus-project">
		<h2>Вам понравился проект?</h2>
		<p>Повседневная практика показывает, что рамки и место обучения кадров в значительной степени обуславливает
			создание существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что
			начало повседневной работы по формированию позиции требуют от нас анализа направлений прогрессивного
			развития.”</p>
	</div>
</div>
<main role="main">
	<div class="container">
		<?php get_footer(); ?>

<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _mbbasetheme
 */
?>

<div class="we-mobile">
	<div class="col-group">
		<section class="aboutus">
			<header>
				<hgroup>
					<h2>О нас</h2>
					<h3>Немного о нас, кто мы и чем занимаемся</h3>
				</hgroup>
			</header>
			<aside>
				<p>Повседневная практика показывает, что консультация с широким активом требуют определения и уточнения модели развития. Идейные соображения высшего порядка, а также укрепление и развитие структуры требуют от нас анализа форм развития. С другой стороны постоянное информационно-пропагандистское обеспечение нашей деятельности в значительной степени обуславливает создание соответствующий условий активизации. Не следует, однако забывать, что постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Товарищи! укрепление и развитие структуры в значительной степени обуславливает создание существенных финансовых и административных условий. Разнообразный и богатый опыт рамки и место обучения кадров обеспечивает широкому кругу (специалистов) участие в формировании системы обучения кадров, соответствует насущным потребностям.</p>
			</aside>
		</section>

		<section class="itsme">
			<header>
				<hgroup>
					<h2>Это мы</h2>
					<h3>Ребята, которые делают действительно классные вещи</h3>
				</hgroup>
			</header>
			<aside>
				<div class="itsme-list col-group">
					<div class="item-itsme col-4">
						<a id="itsme0" class="itsme-popup" data-item="0" href="#animatedModal">
							<div class="img-wrap">
								<img src="/wp-content/themes/devdigital-mobile/assets/images/page-we/sergey.png" alt="Сергей">
							</div>
						</a>
					</div>
					<div class="item-itsme col-4">
						<a id="itsme1" class="itsme-popup" data-item="1" href="#animatedModal">
							<div class="img-wrap">
								<img src="/wp-content/themes/devdigital-mobile/assets/images/page-we/sergey.png" alt="Константин">
							</div>
						</a>
					</div>
					<div class="item-itsme col-4">
						<a id="itsme2" class="itsme-popup" data-item="2" href="#animatedModal">
							<div class="img-wrap">
								<img src="/wp-content/themes/devdigital-mobile/assets/images/page-we/alex.png" alt="Алексей">
							</div>
						</a>
					</div>
					<div class="item-itsme col-4">
						<a id="itsme3" class="itsme-popup" data-item="3" href="#animatedModal">
							<div class="img-wrap">
								<img src="/wp-content/themes/devdigital-mobile/assets/images/page-we/sveta.png" alt="Света">
							</div>
						</a>
					</div>
					<div class="item-itsme col-4">
						<a id="itsme4" class="itsme-popup" data-item="4" href="#animatedModal">
							<div class="img-wrap">
								<img src="/wp-content/themes/devdigital-mobile/assets/images/page-we/dima.png" alt="Дима">
							</div>
						</a>
					</div>
				</div>
				<div id="animatedModal" class="animated animatedModal-off zoomOut" >
					<div id="closebt-container" class="close-animatedModal">
						<img class="closebt" src="http://devdigital.pro/wp-content/themes/devdigital/assets/images/closebt.svg">
					</div>
					<div class="modal-content">
						<div class="itsme-more hidden" id="itsme-more-0">
							<div class="item__description left"><p class="full-name">Сергей</p><p class="post"></p><p class="description"></p></div>
							<div class="item__image"><img src="http://devdigital.pro/wp-content/uploads/2017/01/DSC6177-519x1024.png"> </div>
							<div class="item__description right"><p class="education"></p></div>
						</div>
						<div class="itsme-more hidden" id="itsme-more-1">
							<div class="item__description left "><p class="full-name">Константин</p><p class="post"></p><p class="description"></p></div>
							<div class="item__image"><img src="http://devdigital.pro/wp-content/uploads/2017/01/DSC6174-557x1024.png"> </div>
							<div class="item__description right"><p class="education"></p></div>
						</div>
						<div class="itsme-more hidden" id="itsme-more-2">
							<div class="item__description left"><p class="full-name">Алексей</p><p class="post"></p><p class="description"></p></div>
							<div class="item__image"><img src="http://devdigital.pro/wp-content/uploads/2017/01/DSC6183-520x1024.png"> </div>
							<div class="item__description right"><p class="education"></p></div>
						</div>
						<div class="itsme-more hidden" id="itsme-more-3">
							<div class="item__description left"><p class="full-name">Светлана</p><p class="post"></p><p class="description"></p></div>
							<div class="item__image"><img src="http://devdigital.pro/wp-content/uploads/2017/01/DSC6201-499x1024.png"> </div>
							<div class="item__description right"><p class="education"></p></div>
						</div>
						<div class="itsme-more hidden" id="itsme-more-4">
							<div class="item__description left col-md-4"><p class="full-name">(Eng) Дмитрий</p><p class="post"></p><p class="description"></p></div>
							<div class="item__image col-md-4"><img src="http://devdigital.pro/wp-content/uploads/2017/01/DSC6214-482x1024.png"> </div>
							<div class="item__description right col-md-4"><p class="education"></p></div>
						</div>
					</div>
				</div>
			</aside>
		</section>

		<section class="certificates">
			<header>
				<hgroup>
					<h2>Сертификаты</h2>
					<h3>То, чем мы можем гордиться</h3>
				</hgroup>
			</header>
			<aside>
				<div class="certificates-list col-group">
					<div class="item col-4">
						<a class="popup-gallery" href="" title="Сертификат 1С профессионал" alt="Сертификат 1С профессионал">
							<div class="item__image" style="background-image: url('')"></div>
							<h4>Сертификат 1С профессионал</h4>
						</a>
					</div>
					<div class="item col-4">
						<a class="popup-gallery" href="" title="Сертификат google digital marketing lab" alt="Сертификат google digital marketing lab">
							<div class="item__image" style="background-image: url('')"></div>
							<h4>Сертификат google digital marketing lab</h4>
						</a>
					</div>
					<div class="item col-4">
						<a class="popup-gallery" href="" title="Сертификат google digital marketing lab" alt="Сертификат google digital marketing lab">
							<div class="item__image" style="background-image: url('')"></div>
							<h4>Сертификат google digital marketing lab</h4>
						</a>
					</div>
					<div class="item col-4">
						<a class="popup-gallery" href="" title="Сертификат 3" alt="Сертификат 3">
							<div class="item__image" style="background-image: url('')"></div>
							<h4>Сертификат 3</h4>
						</a>
					</div>
				</div>
			</aside>
		</section>
	</div>



</div>

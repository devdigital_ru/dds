<div class="col-4 col-mb-12 item-project">
	<?php
	if(get_field('image_preview')){
		$image_preview = get_field('image_preview');
		$image_preview = $image_preview['url'];
	}else{
		$image_preview = get_field('project_preview');
		$image_preview = $image_preview["sizes"]["project-preview"];
	}
	?>
	<div class="image" style="background-image: url('<?= $image_preview ?>')">
		<a href="<?= get_the_permalink(); ?>" title="<?= get_the_title() ?>">
			<div class="description">
				<h2><?= get_the_title() ?></h2>
				<h3><?= get_field('project_description') ?></h3>
			</div>
		</a>
	</div>
</div>
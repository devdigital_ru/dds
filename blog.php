<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `devdigital_dd-modx`
//

// `devdigital_dd-modx`.`modx_site_content`
$modx_site_content = array(
	array('id' => '29','pagetitle' => 'Переводчик','publishedon' => '1380622620'),
	array('id' => '30','pagetitle' => 'Yet another Conference 2013. Конкурс по ускорению отрисовки страницы','publishedon' => '1380622620'),
	array('id' => '31','pagetitle' => 'Мы на «Yet another Conference 2013» в Москве','publishedon' => '1380694800'),
	array('id' => '32','pagetitle' => 'Цитаты «Yet another Conference 2013»','publishedon' => '1380963120'),
	array('id' => '33','pagetitle' => 'Немного мыслей на тему YaC2013','publishedon' => '1380963660'),
	array('id' => '34','pagetitle' => 'Задачи и ребусы от ШАД','publishedon' => '1380963960'),
	array('id' => '35','pagetitle' => 'Yet another Conference 2013. Конкурс по ускорению отрисовки страницы. Наши результаты.','publishedon' => '1381132860'),
	array('id' => '36','pagetitle' => 'Брендбук для «Devdigital»','publishedon' => '1381831980'),
	array('id' => '37','pagetitle' => 'УЭК в работе','publishedon' => '1382083260'),
	array('id' => '38','pagetitle' => 'Библиотека веб-студии «Devdigital»','publishedon' => '1382613600'),
	array('id' => '39','pagetitle' => 'Лечение ??? вместо кириллицы на MODx','publishedon' => '1382957880'),
	array('id' => '40','pagetitle' => 'Решение самого сложного ребуса от ШАД на YaC2013','publishedon' => '1383210060'),
	array('id' => '41','pagetitle' => 'Восстановление работы ExpressCache и Intel Rapid на Windows','publishedon' => '1386325200'),
	array('id' => '42','pagetitle' => 'Удаление “лишних” папок из «Этот компьютер» в Windows 8.1','publishedon' => '1386423120'),
	array('id' => '43','pagetitle' => 'Вторая задача с YaC2013. Perla negra.','publishedon' => '1388140440'),
	array('id' => '44','pagetitle' => 'Третья задача с YaC2013. Четыре путника в Сахаре.','publishedon' => '1388329740'),
	array('id' => '116','pagetitle' => '2GIS и&nbsp;Foursquare','publishedon' => '1401717600'),
	array('id' => '117','pagetitle' => 'Галереи сайтов и зарубежные конкурсы','publishedon' => '1401978060'),
	array('id' => '119','pagetitle' => 'Как не стоит создавать сайты. Никогда.','publishedon' => '1413381600'),
	array('id' => '120','pagetitle' => 'Цитаты из&nbsp;книги Майка Монтейро&nbsp;&laquo;Дизайн &mdash; это работа&raquo;','publishedon' => '1402050720'),
	array('id' => '122','pagetitle' => 'Полезные инструменты. Выпуск 1.','publishedon' => '1402906860'),
	array('id' => '123','pagetitle' => 'Полезные инструменты. Выпуск 2.','publishedon' => '1403509632'),
	array('id' => '124','pagetitle' => 'Полезные инструменты. Выпуск 3.','publishedon' => '1404165600'),
	array('id' => '125','pagetitle' => 'Полезные инструменты. Выпуск 4.','publishedon' => '1404684000'),
	array('id' => '126','pagetitle' => 'Мы на Картах Google','publishedon' => '1402917660'),
	array('id' => '127','pagetitle' => 'Полезные инструменты. Выпуск 5.','publishedon' => '1405375200'),
	array('id' => '128','pagetitle' => 'Полезные инструменты. Выпуск 6.','publishedon' => '1405934880'),
	array('id' => '2328','pagetitle' => 'Drupal 7 фильтр атрибутов Ubercart во views. Или копирование атрибутов ubercart в таксономию.','publishedon' => '1422301140'),
	array('id' => '2329','pagetitle' => 'Drupal 7 добавление в избранное с использованием AJAX','publishedon' => '1410264900'),
	array('id' => '2330','pagetitle' => 'Генерация страниц в MODx Revolution','publishedon' => '1406930400'),
	array('id' => '2331','pagetitle' => 'Полезные инструменты. Выпуск 7.','publishedon' => '1406535000'),
	array('id' => '2370','pagetitle' => 'Полезные инструменты. Выпуск 8.','publishedon' => '1407103200'),
	array('id' => '2386','pagetitle' => 'Полезные инструменты. Выпуск 9.','publishedon' => '1407708000'),
	array('id' => '2392','pagetitle' => 'Полезные инструменты. Выпуск 10.','publishedon' => '1408312800'),
	array('id' => '2393','pagetitle' => 'Полезные инструменты. Выпуск 11.','publishedon' => '1409090400'),
	array('id' => '2394','pagetitle' => 'Полезные инструменты. Выпуск 12.','publishedon' => '1409522400'),
	array('id' => '2395','pagetitle' => 'Полезные инструменты. Выпуск 13.','publishedon' => '1410732000'),
	array('id' => '2408','pagetitle' => 'Wordpress динамическая сортировка записей и пагинация страниц','publishedon' => '1406187720'),
	array('id' => '2409','pagetitle' => 'Новые экраны и проблемы iPhone 6','publishedon' => '1410435600'),
	array('id' => '2410','pagetitle' => 'Digital Marketing Lab от Google','publishedon' => '1411483200'),
	array('id' => '2411','pagetitle' => 'Полезные инструменты. Выпуск 14.','publishedon' => '1411979400'),
	array('id' => '2412','pagetitle' => 'Полезные инструменты. Выпуск 15.','publishedon' => '1412843400'),
	array('id' => '2446','pagetitle' => 'Едем на Yet another Conference 2014','publishedon' => '1414573200'),
	array('id' => '2447','pagetitle' => 'Немного мыслей на тему YaC2014','publishedon' => '1414863000'),
	array('id' => '2448','pagetitle' => 'Полезные инструменты. Выпуск 16.','publishedon' => '1415352420'),
	array('id' => '2496','pagetitle' => 'Итоги 2014 года','publishedon' => '1422793800'),
	array('id' => '2515','pagetitle' => 'IT конференция в БГТУ им. В.Г. Шухова','publishedon' => '1433529540')
);
